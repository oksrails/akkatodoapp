db.createUser({
    user: 'mongoadmin',
    pwd: 'secret',
    roles: [
      {
        role: 'dbOwner',
        db: 'akkaToDo',
      },
    ],
  });