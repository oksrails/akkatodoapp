﻿using FluentAssertions;
using System;
using Xunit;

namespace AkkaToDoApp.ProjectTasks.Domain.Tests
{
    public class ProjectTaskDomainTests
    {
        ProjectTask _task;

        public ProjectTaskDomainTests()
        {
            _task = new ProjectTask(
                ProjectTaskId.FromGuid(Guid.NewGuid()),
                ProjectId.FromGuid(Guid.NewGuid()));
        }

        [Fact]
        public void Change_task_title_success_test()
        {
            var taskTitleNew = "New test task title";
            var taskTitleChanged = new Events.V1.ProjectTaskTitleChanged(_task.Id, _task.ProjectId ?? Guid.Empty, taskTitleNew);
            _task.Apply(taskTitleChanged);

            _task.TaskTitle?.Value.Should().Be(taskTitleNew);
        }

        [Fact]
        public void Change_task_description_success_test()
        {
            var taskDescriptionNew = TaskDescription.FromString("New test task description");
            var taskDescriptionChanged = new Events.V1.ProjectTaskDescriptionChanged(_task.Id, _task.ProjectId ?? Guid.Empty, taskDescriptionNew);
            _task.Apply(taskDescriptionChanged);

            _task.TaskDescription?.Value.Should().Be(taskDescriptionNew);
        }

        [Fact]
        public void Change_task_type_success_test()
        {
            var taskType = 0;
            var taskTypeChanged = new Events.V1.ProjectTaskTypeChanged(_task.Id, _task.ProjectId ?? Guid.Empty, taskType);
            _task.Apply(taskTypeChanged);
            
            _task.TaskType.Should().Be((ProjectTaskType)taskType);
        }

        [Fact]
        public void Change_task_estimation_success_test()
        {
            decimal estimation = 13.5m;
            var taskEstimationChanged = new Events.V1.ProjectTaskEstimationChanged(_task.Id, _task.ProjectId ?? Guid.Empty, estimation);
            _task.Apply(taskEstimationChanged);

            _task.TaskEstimation?.Value.Should().Be(estimation);
        }

        [Fact]
        public void Assign_project_task_success_test()
        {
            Guid userId = Guid.NewGuid();
            var taskAssignmentChanged = new Events.V1.ProjectTaskAssigned(_task.Id, _task.ProjectId ?? Guid.Empty, userId);
            _task.Apply(taskAssignmentChanged);

            _task.AssignedUserId?.Value.Should().Be(userId);
        }

        [Fact]
        public void Take_project_task_to_work_success_test()
        {
            var workStarted = new Events.V1.ProjectTaskWorkStarted(_task.Id, _task.ProjectId ?? Guid.Empty);
            _task.Apply(workStarted);

            _task.TaskStatus.Should().Be(ProjectTaskStatus.InProgress);
        }

        [Fact]
        public void Complete_project_task_success_test()
        {
            var taskCompleted = new Events.V1.ProjectTaskCompleted(_task.Id, _task.ProjectId ?? Guid.Empty);
            _task.Apply(taskCompleted);

            _task.TaskStatus.Should().Be(ProjectTaskStatus.Done);
        }
    }
}
