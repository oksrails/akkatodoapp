﻿using System.Text;

namespace AkkaToDoApp.ProjectTasks.Domain.Tests
{
    public static class TestTools
    {
        public static string GenerateRandomString(int length)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 0; i <= length; i++)
                result.Append("A");

            return result.ToString();
        }
    }
}
