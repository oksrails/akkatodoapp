using Xunit;
using System;

namespace AkkaToDoApp.Projects.Domain.Tests
{
    public class UserIdTests
    {
        [Fact]
        public void Create_user_id_from_string_with_emty_string_exception()
        {
            string userId = "";

            Assert.Throws<ArgumentException>(() => UserId.FromString(userId));
        }

        [Fact]
        public void Create_user_id_from_string_with_default_value_exception()
        {
            string ownerId = Guid.Empty.ToString();

            Assert.Throws<ArgumentException>(() => UserId.FromString(ownerId));
        }

        [Fact]
        public void Create_user_id_from_guid_with_default_value_exception()
        {
            Guid ownerId = Guid.Empty;

            Assert.Throws<ArgumentException>(() => UserId.FromGuid(ownerId));
        }

        [Fact]
        public void Create_user_id_from_string_succes()
        {
            string userId = Guid.NewGuid().ToString();
            UserId user = UserId.FromString(userId);

            Assert.NotNull(user);
            Assert.Equal(userId, user.ToString());
        }

        [Fact]
        public void Create_user_id_from_guid_succes()
        {
            Guid userId = Guid.NewGuid();
            UserId user = UserId.FromGuid(userId);

            Assert.NotNull(user);
            Assert.Equal(userId, user.Value);
        }
    }
}