﻿using System;
using Xunit;

namespace AkkaToDoApp.Projects.Domain.Tests
{
    public class ProjectDescriptionTests
    {
        [Fact]
        public void Create_project_description_succes()
        {
            string projectDescriptionInput = "Test project description";

            ProjectDescription projectDescription = ProjectDescription.FromString(projectDescriptionInput);

            Assert.NotNull(projectDescription);
            Assert.Equal(projectDescriptionInput, projectDescription);
        }

        [Fact]
        public void Create_project_description_with_empty_string_exception()
        {
            string projectDescriptionInput = "";

            Assert.Throws<ArgumentException>(() => ProjectDescription.FromString(projectDescriptionInput));
        }

        [Fact]
        public void Create_project_description_with_less_then_10_string_exception()
        {
            string projectDescriptionInput = "A";

            Assert.Throws<ArgumentException>(() => ProjectDescription.FromString(projectDescriptionInput));
        }
    }
}
