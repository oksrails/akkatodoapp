﻿using System;
using Xunit;
using FluentAssertions;

namespace AkkaToDoApp.Projects.Domain.Tests
{
    public class ProjectDomainTests
    {
        private readonly Project _project;

        public ProjectDomainTests()
        {
            _project = new Project(
                ProjectId.FromGuid(Guid.NewGuid()),
                UserId.FromGuid(Guid.NewGuid()));
        }

        [Fact]
        public void Project_title_change_success_test()
        {
            string newProjectTitle = "New test project title";
            Events.V1.ProjectTitleChanged projectTitleChanged = new Events.V1.ProjectTitleChanged(_project.Id, newProjectTitle);

            _project.Apply(projectTitleChanged);

            _project.ProjectTitle?.Value.Should().Be(newProjectTitle);
        }

        [Fact]
        public void Project_description_change_success_test()
        {
            string newProjectDescription = "New test project description";
            Events.V1.ProjectDescriptionChanged projectDescriptionChanged = new Events.V1.ProjectDescriptionChanged(_project.Id, newProjectDescription);

            _project.Apply(projectDescriptionChanged);

            _project.ProjectDescription?.Value.Should().Be(newProjectDescription);
        }
    }
}
