﻿namespace AkkaToDoApp.Common.Domain
{
    public record AggregateId
    {
        protected AggregateId(Guid value)
        {
            if (value == Guid.Empty)
                throw new ArgumentNullException(
                    nameof(value),
                    "The Id cannot be empty");

            Value = value;
        }

        public Guid Value { get; }

        public static implicit operator Guid(AggregateId self) => self.Value;

        public override string ToString() => Value.ToString();

        public string ToString(string format) => Value.ToString(format);
    }
}
