﻿using System.Collections.Immutable;

namespace AkkaToDoApp.Common.Domain
{
    public abstract class AggregateRoot
    {
        readonly IImmutableList<object> _changes = ImmutableList.Create<object>();

        public Guid Id { get; protected set; }

        public int Version { get; private set; } = -1;

        public void Apply(object evt)
        {
            When(evt);
            EnsureValidState();
            _changes.Add(evt);
        }

        public void Load(IEnumerable<object> events)
        {
            foreach (var @event in events)
            {
                When(@event);
                Version++;
            }
        }

        protected abstract void When(object evt);

        protected abstract void EnsureValidState();

        public IEnumerable<object> GetChanges() => _changes.AsEnumerable();

        public void ClearChanges() => _changes.Clear();
    }
}