﻿namespace AkkaToDoApp.Projects
{
    public record QueryModels
    {
        public record FetchProjectProjections : IProjectQuery
        {
            public static FetchProjectProjections Instance = new FetchProjectProjections();
            private FetchProjectProjections() { }
        }
        public record GetProjectHistoryById : IProjectIdQuery
        {
            public Guid ProjectId { get; init; }
        }

        public record GetProjectProjectionById : IProjectIdQuery
        {
            public Guid ProjectId { get; init; }
        }
    }
}
