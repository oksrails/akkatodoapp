﻿namespace AkkaToDoApp.Projects
{
    public interface IProjectQuery { }
    public interface IProjectIdQuery
    {
        Guid ProjectId { get; init; }
    }
}
