﻿namespace AkkaToDoApp.Projects
{
    public record Commands
    {
        public record FetchProjectHistory : IProjectCommand
        {
            public Guid ProjectId { get; init; }

            public FetchProjectHistory(Guid projectId)
            {
                ProjectId = projectId;
            }
        }

        public record PublishProjection : IProjectCommand
        {
            public Guid ProjectId { get; init; }
            public PublishProjection(Guid projectId)
            {
                ProjectId = projectId;
            }
        }

        public sealed class Ping : IEquatable<Ping>, IProjectCommand
        {
            public Guid ProjectId { get; init; }

            public Ping(Guid projectId)
            {
                ProjectId = projectId;
            }

            public override bool Equals(object? obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                return obj is Ping other && Equals(other);
            }

            public bool Equals(Ping? other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Equals(ProjectId, other.ProjectId);
            }

            public override int GetHashCode()
            {
                return ProjectId.GetHashCode();
            }

            public static bool operator ==(Ping left, Ping right)
            {
                return Equals(left, right);
            }

            public static bool operator !=(Ping left, Ping right)
            {
                return !Equals(left, right);
            }
        }

        public record V1
        {
            public record CreateProject : IProjectCommand
            {
                public Guid ProjectId { get; init; }
                public string ProjectTitle { get; init; }
                public string ProjectDescription { get; init; }
                public Guid ProjectOwnerId { get; init; }

                public CreateProject(
                    Guid projectId,
                    string projectTitle,
                    string projectDescription,
                    Guid projectOwnerId)
                {
                    ProjectId = projectId;
                    ProjectTitle = projectTitle;
                    ProjectDescription = projectDescription;
                    ProjectOwnerId = projectOwnerId;
                }

                public override string ToString() => $"{nameof(CreateProject)}";
            }

            public record ChangeProjectTitle : IProjectCommand
            {
                public Guid ProjectId { get; init; }
                public string ProjectTitle { get; init; }

                public ChangeProjectTitle(
                    Guid projectId,
                    string projectTitle)
                {
                    ProjectId = projectId;
                    ProjectTitle = projectTitle;
                }

                public override string ToString() => $"{nameof(ChangeProjectTitle)}";
            }

            public record ChangeProjectDescription : IProjectCommand
            {
                public Guid ProjectId { get; init; }
                public string ProjectDescription { get; init; }

                public ChangeProjectDescription(
                    Guid projectId,
                    string projectDescription)
                {
                    ProjectId = projectId;
                    ProjectDescription = projectDescription;
                }

                public override string ToString() => $"{nameof(ChangeProjectDescription)}";
            }
        }
    }
}