﻿namespace AkkaToDoApp.Projects
{
    public interface IProjectCommand
    {
        Guid ProjectId { get; init; }
    }
}
