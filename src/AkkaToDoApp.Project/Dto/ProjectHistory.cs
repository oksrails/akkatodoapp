﻿using AkkaToDoApp.Projects.Domain;
using System.Collections.Immutable;

namespace AkkaToDoApp.Projects.Dto
{
    public class ProjectHistory
    {
        public Guid ProjectId { get; init; }
        public Project ProjectCurentState { get; }
        public ImmutableHashSet<object> ProjectEvents { get; }

        public ProjectHistory(Guid projectId, ImmutableHashSet<object> projectEvents)
        {
            ProjectId = projectId;
            ProjectEvents = projectEvents;
            ProjectCurentState = new Project();
            if (projectEvents.Any())
            {
                ProjectCurentState.Load(projectEvents);
            }
        }

        public ProjectHistory AppendProjectEvent(IProjectEvent projectEvent)
        {
            if (!projectEvent.ProjectId.Equals(ProjectId))
                throw new ArgumentException($"Expected project id: {ProjectId}, but received {projectEvent.ProjectId}", nameof(projectEvent));

            return new ProjectHistory(ProjectId, ProjectEvents.Add(projectEvent));
        }
    }
}
