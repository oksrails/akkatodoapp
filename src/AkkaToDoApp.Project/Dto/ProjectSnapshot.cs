﻿namespace AkkaToDoApp.Projects.Dto
{
    public record ProjectSnapshot
    {
        public Guid ProjectId { get; }
        public string? ProjectTitle { get; }
        public string? ProjectDescription { get; }
        public Guid ProjectOwner { get; }
        public long QueryOffset { get; }
        public ProjectSnapshot(
            Guid projectId,
            string projectTitle,
            string projectDescription,
            Guid projectOwner,
            long queryOffset)
        {
            ProjectId = projectId;
            ProjectTitle = projectTitle;
            ProjectDescription = projectDescription;
            ProjectOwner = projectOwner;
            QueryOffset = queryOffset;
        }
    }
}
