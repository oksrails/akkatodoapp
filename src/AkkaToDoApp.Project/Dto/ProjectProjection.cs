﻿namespace AkkaToDoApp.Projects.Dto
{
    public record ProjectProjection
    {
        public Guid ProjectId { get; }
        public string ProjectTitle { get; }
        public string ProjectDescription { get; }
        public string ProjectOwnerName { get; }

        public ProjectProjection(
            Guid projectId,
            string projectTitle,
            string projectDescription,
            string projectOwnerName)
        {
            ProjectId = projectId;
            ProjectTitle = projectTitle;
            ProjectDescription = projectDescription;
            ProjectOwnerName = projectOwnerName;
        }
    }
}
