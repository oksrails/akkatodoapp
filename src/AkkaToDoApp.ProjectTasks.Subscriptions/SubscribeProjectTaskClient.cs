﻿using Akka.Actor;

namespace AkkaToDoApp.ProjectTasks.Subscriptions
{
    public class SubscribeProjectTaskClient
    {
        public IActorRef Subscriber { get; init; }

        public SubscribeProjectTaskClient(IActorRef subscriber)
        {
            Subscriber = subscriber;
        }
    }
}