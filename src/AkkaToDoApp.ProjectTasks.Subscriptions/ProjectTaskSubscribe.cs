﻿using Akka.Actor;

namespace AkkaToDoApp.ProjectTasks.Subscriptions
{
    public class ProjectTaskSubscribe
    {
        public Guid ProjectTaskId { get; init; }
        public IActorRef Subscriber { get; init; }

        public ProjectTaskSubscribe(Guid projectTaskId, IActorRef subscriber)
        {
            ProjectTaskId = projectTaskId;
            Subscriber = subscriber;
        }
    }
}
