﻿using Akka.Actor;
using Akka.Event;
using Akka.Cluster.Tools.PublishSubscribe;

namespace AkkaToDoApp.ProjectTasks.Actors
{
    public sealed class ProjectTaskMasterActor : ReceiveActor
    {
        ILoggingAdapter _log = Context.GetLogger();

        public ProjectTaskMasterActor()
        {
            Receive<IProjectTaskCommand>(s =>
            {
                var projectTaskActor = Context.Child($"{s.ProjectTaskId.ToString()}-projectTask").GetOrElse(() => StartChild(s.ProjectTaskId));
                projectTaskActor.Forward(s);
            });
        }

        protected override void PreStart()
        {
            _log.Info($"Starting project task master actor...");
            base.PreStart();
        }

        protected override void PostStop()
        {
            _log.Info($"Project task master actor has been stopped");
            base.PostStop();
        }

        private IActorRef StartChild(Guid projectTaskId)
        {
            var mediator = DistributedPubSub.Get(Context.System).Mediator;
            return Context.ActorOf(Props.Create(() => new ProjectTaskActor(projectTaskId, mediator)), $"{projectTaskId.ToString()}-projectTask");
        }
    }
}
