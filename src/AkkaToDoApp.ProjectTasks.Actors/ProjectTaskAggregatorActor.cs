﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;
using AkkaToDoApp.ProjectTasks.Domain;
using AkkaToDoApp.ProjectTasks.Dto;
using System.Collections.Immutable;
using static AkkaToDoApp.ProjectTasks.Commands;

namespace AkkaToDoApp.ProjectTasks.Actors
{
    public class ProjectTaskAggregatorActor : ReceivePersistentActor
    {
        private readonly ILoggingAdapter _log = Context.GetLogger();
        public override string PersistenceId { get; }
        private Guid ProjectTaskId { get; set; }
        private readonly IEventsByTagQuery _eventsByTag;
        private readonly ICurrentEventsByTagQuery _currentEventsByTag;
        IActorRef _mediator;
        public long QueryOffset { get; private set; }
        private ProjectTaskHistory ProjectTaskHistory;
        private ProjectTask ProjectTask;

        public ProjectTaskAggregatorActor(
            string persistenceId,
            IEventsByTagQuery eventsByTag,
            ICurrentEventsByTagQuery currentEventsByTag)
        {
            PersistenceId = persistenceId;
            ProjectTaskId = Guid.Parse(persistenceId);
            _eventsByTag = eventsByTag;
            _currentEventsByTag = currentEventsByTag;
            _mediator = DistributedPubSub.Get(Context.System).Mediator;
            ProjectTaskHistory = new ProjectTaskHistory(ProjectTaskId, ImmutableHashSet<object>.Empty);
            ProjectTask = new ProjectTask();

            Receives();
            Commands();
        }

        protected override void PreStart()
        {
            _log.Info($"Starting project task aggregator for persistence {PersistenceId}...");
            InitializeProjectTaskHistory();
        }

        protected override void PostStop()
        {
            _log.Info($"Project task aggregator for persistence {PersistenceId} has been stopped");
        }

        private void Receives()
        {
            Recover<SnapshotOffer>(s =>
            {
                if (s.Snapshot is ProjectTaskSnapshot snapshot)
                {
                    RecoverAggregatedData(snapshot);
                }
            });

            Recover<ProjectTaskSnapshot>(s =>
            {
                RecoverAggregatedData(s);
            });
        }

        private void Commands()
        {
            Command<EventEnvelope>(e =>
            {
                _log.Debug($"Received project task event {e.Event.ToString()}");
                if (e.Event is IProjectTaskEvent @event)
                {
                    ProjectTaskHistory.AppendProjectTaskEvent(@event);
                }
            });

            Command<V1.FetchProjectTaskHistory>(f => Sender.Tell(ProjectTaskHistory));

            Command<V1.FetchProjectTaskCurrentState>(f => Sender.Tell(ProjectTask));
        }

        protected override void OnReplaySuccess()
        {
            var mat = Context.Materializer();
            var self = Self;

            _eventsByTag
                .EventsByTag(PersistenceId, Offset.Sequence(QueryOffset))
                .RunWith(Sink.ActorRef<EventEnvelope>(self, UnexpectedEndOfSream.Instance), mat);

            base.OnReplaySuccess();
        }

        private void InitializeProjectTaskHistory()
        {
            var mat = Context.Materializer();
            var events = _currentEventsByTag
                .CurrentEventsByTag(PersistenceId, Offset.NoOffset())
                .Select(e => e.Event)
                .RunAggregate(
                    ImmutableHashSet<object>.Empty,
                    (acc, c) => acc.Add(c),
                    mat)
                .Result;

            ProjectTaskHistory = new ProjectTaskHistory(ProjectTaskId, events);
        }

        private void RecoverAggregatedData(ProjectTaskSnapshot snapshot)
        {
            ProjectTask = new ProjectTask(
                snapshot.ProjectTaskId, 
                snapshot.ProjectId, 
                snapshot.TaskTitle, 
                snapshot.TaskDescription, 
                snapshot.TaskType, 
                snapshot.TaskEstimation ?? decimal.Zero, 
                snapshot.TaskStatus, 
                snapshot.AssignedUserId ?? Guid.Empty);
            QueryOffset = snapshot.QueryOffset;
        }
    }
}
