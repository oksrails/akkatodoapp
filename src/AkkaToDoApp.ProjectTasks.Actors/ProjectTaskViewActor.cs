﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence.Query;
using AkkaToDoApp.Common.Subscriptions;
using AkkaToDoApp.Projects.Dto;
using AkkaToDoApp.ProjectTasks.Domain;
using AkkaToDoApp.ProjectTasks.Dto;
using AkkaToDoApp.Users.Domain;
using System.Collections.Immutable;
using static AkkaToDoApp.Projects.QueryModels;
using static AkkaToDoApp.ProjectTasks.Commands;
using static AkkaToDoApp.ProjectTasks.QueryModels;
using static AkkaToDoApp.Users.QueryModels;
using static AkkaToDoApp.Common.Actors.Messages;

namespace AkkaToDoApp.ProjectTasks.Actors
{
    public class ProjectTaskViewActor : ReceiveActor, IWithUnboundedStash
    {
        ILoggingAdapter _log = Context.GetLogger();
        public IStash Stash { get; set; }
        private readonly IActorRef _userViewActor;
        private readonly IActorRef _projectViewActor;
        private readonly IActorRef _mediator;
        private IActorRef? _tickerEntity;
        private IActorRef _actorGateway;
        private ProjectTaskProjection? TaskProjection { get; set; }
        private ProjectTaskHistory TaskHistory { get; set; }
        private long QueryOffset = 0;
        private readonly string _projectTaskTopic;
        public Guid ProjectTaskId { get; }
        public ProjectTaskViewActor(
            Guid projectTaskId,
            IActorRef userViewActor,
            IActorRef projectViewActor,
            IActorRef actorGateway,
            IActorRef mediator)
        {
            ProjectTaskId = projectTaskId;
            _userViewActor = userViewActor;
            _projectViewActor = projectViewActor;
            _actorGateway = actorGateway;
            _mediator = mediator;
            _projectTaskTopic = TopicHelper.ProjectTaskTopic(ProjectTaskId);
            TaskHistory = new ProjectTaskHistory(ProjectTaskId, ImmutableHashSet<object>.Empty);

            WaitingForProjectActor();
        }

        protected override void PreStart()
        {
            _log.Info($"Starting project task view actor...");

            Context.SetReceiveTimeout(TimeSpan.FromSeconds(10));
            _projectViewActor.Tell(new Request.CheckRunningRequest());
        }

        protected override void PostStop()
        {
            _log.Info($"Project task view actor has been stopped");
        }

        private void WaitingForVolume()
        {
            Receive<ProjectTaskHistory>(h =>
            {
                TaskHistory = h;
                GenerateTaskProjection().Wait();
                _tickerEntity = Self;
                _mediator.Tell(new Subscribe(_projectTaskTopic, Self));
            });

            Receive<SubscribeAck>(ack =>
            {
                _log.Info($"Subscribed to {_projectTaskTopic} - ready for real time processing");
                Become(Processing);
                Stash.UnstashAll();
                Context.Watch(_tickerEntity);
                Context.SetReceiveTimeout(null);
            });

            Receive<ReceiveTimeout>(timeout =>
            {
                _log.Warning($"Received no initial values for project task id {ProjectTaskId} from source of truth after 5s. Retrying...");
                _actorGateway.Tell(new V1.FetchProjectTaskHistory(ProjectTaskId));
            });

            ReceiveAny(_ => Stash.Stash());
        }

        private void WaitingForProjectActor()
        {
            Receive<Response.CheckRunningResponse>(r =>
            {
                if (r.IsRunning)
                {
                    Context.SetReceiveTimeout(TimeSpan.FromSeconds(5));
                    _actorGateway.Tell(new V1.FetchProjectTaskHistory(ProjectTaskId));
                    Become(WaitingForVolume);
                }
                else
                {
                    _log.Warning("Project view master actor is not started yet. Retrying...");
                    _projectViewActor.Tell(new Request.CheckRunningRequest());
                }
            });

            Receive<ReceiveTimeout>(timeout =>
            {
                _log.Warning("Received no answer from project view master actor. Retrying...");
                _projectViewActor.Tell(new Request.CheckRunningRequest());
            });
        }

        private void Processing()
        {
            Receive<IProjectTaskEvent>(e =>
            {
                TaskHistory = TaskHistory.AppendProjectTaskEvent(e);
                GenerateTaskProjection().Wait();
            });

            Receive<EventEnvelope>(e =>
            {
                if (e.Event is IProjectTaskEvent taskEvent)
                {
                    TaskHistory = TaskHistory.AppendProjectTaskEvent(taskEvent);
                    GenerateTaskProjection().Wait();

                    if (e.Offset is Sequence s)
                    {
                        QueryOffset = s.Value;
                    }
                }
            });

            Receive<GetProjectTaskHistoryById>(q =>
            {
                if (q.ProjectTaskId.Equals(ProjectTaskId))
                {
                    Sender.Tell(TaskHistory);
                }
                else
                {
                    _log.Warning($"Expected project task id: {ProjectTaskId}, but received {q.ProjectTaskId}");
                }
            });

            Receive<GetProjectTaskProjectionById>(q =>
            {
                if (q.ProjectTaskId.Equals(ProjectTaskId))
                {
                    Sender.Tell(TaskProjection);
                }
                else
                {
                    _log.Warning($"Expected project task id: {ProjectTaskId}, but received {q.ProjectTaskId}");
                }
            });

            Receive<Ping>(p =>
            {
                _log.Debug($"Pinged via {Sender}");
            });

            Receive<Terminated>(t =>
            {
                if (t.ActorRef.Equals(Self))
                {
                    _log.Info("Source of truth entity terminated. Re-acquiring...");
                    Context.SetReceiveTimeout(TimeSpan.FromSeconds(5));
                    _actorGateway.Tell(new V1.FetchProjectTaskHistory(ProjectTaskId));
                    _mediator.Tell(new Unsubscribe(_projectTaskTopic, Self));
                    Become(WaitingForVolume);
                }
            });
        }

        private async Task GenerateTaskProjection()
        {
            var project = await GetProject(TaskHistory.ProjectTaskCurentState.ProjectId);
            string userName = "";
            if (TaskHistory.ProjectTaskCurentState.AssignedUserId is not null)
            {
                var userProfile = await GetUserProfile(TaskHistory.ProjectTaskCurentState.AssignedUserId);
                userName = userProfile.DisplayName;
            }
            TaskProjection = new ProjectTaskProjection(
                ProjectTaskId, 
                project.ProjectTitle,
                TaskHistory.ProjectTaskCurentState.TaskTitle,
                TaskHistory.ProjectTaskCurentState.TaskDescription,
                TaskHistory.ProjectTaskCurentState.TaskType.ToString(),
                TaskHistory.ProjectTaskCurentState.TaskEstimation.ToString(),
                TaskHistory.ProjectTaskCurentState.TaskStatus.ToString(),
                userName);
        }

        private async Task<ProjectProjection> GetProject(Guid projectId)
        {
            var result = await _projectViewActor.Ask(new GetProjectProjectionById { ProjectId = projectId });
            if (result is ProjectProjection project)
            {
                return project;
            }
            else
            {
                throw new ArgumentException($"Received unrecognized answer from {_projectViewActor.Path}", nameof(result));
            }
        }

        private async Task<UserProfile> GetUserProfile(Guid userProfileId)
        {
            var result = await _userViewActor.Ask(new GetUserProfileByIdQuery { UserProfileId = userProfileId });
            if (result is UserProfile userProfile)
            {
                return userProfile;
            }
            else
            {
                throw new ArgumentException($"Received unrecognized answer from {_userViewActor.Path}", nameof(result));
            }
        }
    }
}
