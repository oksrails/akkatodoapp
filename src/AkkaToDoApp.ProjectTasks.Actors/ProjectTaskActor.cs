﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence;
using AkkaToDoApp.Common.Subscriptions;
using AkkaToDoApp.ProjectTasks.Domain;
using AkkaToDoApp.ProjectTasks.Dto;

namespace AkkaToDoApp.ProjectTasks.Actors
{
    public class ProjectTaskActor : ReceivePersistentActor
    {
        ILoggingAdapter _log = Context.GetLogger();
        private const int SnapshotInterval = 10;
        public override string PersistenceId { get; }
        private Guid ProjectTaskId { get; }
        private string _projectTaskTopic;
        private ProjectTask ProjectTask { get; set; }
        private readonly IActorRef _mediator;
        private long QueryOffset;

        public ProjectTaskActor(Guid projectTaskId, IActorRef mediator)
        {
            ProjectTaskId = projectTaskId;
            PersistenceId = $"{projectTaskId.ToString()}-projectTask";
            ProjectTask = new ProjectTask();
            _projectTaskTopic = TopicHelper.ProjectTaskTopic(ProjectTaskId);
            _mediator = mediator;

            Recovers();
            Commands();
        }

        protected override void PreStart()
        {
            _log.Info($"Project task actor {PersistenceId} has been started");
            base.PreStart();
        }

        protected override void PostStop()
        {
            _log.Info($"Project task actor {PersistenceId} has been stopped");
            base.PostStop();
        }

        private void Recovers()
        {
            Recover<SnapshotOffer>(offer =>
            {
                if (offer.Snapshot is ProjectTaskSnapshot projectTaskSnapshot)
                {
                    ProjectTask = new ProjectTask(
                        projectTaskSnapshot.ProjectTaskId,
                        projectTaskSnapshot.ProjectId,
                        projectTaskSnapshot.TaskTitle,
                        projectTaskSnapshot.TaskDescription,
                        projectTaskSnapshot.TaskType,
                        projectTaskSnapshot.TaskEstimation ?? decimal.Zero,
                        projectTaskSnapshot.TaskStatus,
                        projectTaskSnapshot.AssignedUserId ?? Guid.Empty);
                }
            });

            Recover<IProjectTaskEvent>(e =>
            {
                ProjectTask.Apply(e);
            });
        }

        private void Commands()
        {
            Command<Commands.V1.CreateProjectTask>(c =>
            {
                var projectTaskCreated = new Events.V1.ProjectTaskCreated(c.ProjectTaskId, c.ProjectId);
                ProcessEvent(projectTaskCreated);
            });

            Command<Commands.V1.ChangeProjectTaskTitle>(c =>
            {
                var projectTitleChanged = new Events.V1.ProjectTaskTitleChanged(c.ProjectTaskId, c.ProjectId, c.TaskTitle);
                ProcessEvent(projectTitleChanged);
            });

            Command<Commands.V1.ChangeProjectTaskDescription>(c =>
            {
                var projectTaskDescriptionChanged = new Events.V1.ProjectTaskDescriptionChanged(c.ProjectTaskId, c.ProjectId, c.Description);
                ProcessEvent(projectTaskDescriptionChanged);
            });

            Command<Commands.V1.ChangeProjectTaskEstimation>(c =>
            {
                var projectTaskEstimationChanged = new Events.V1.ProjectTaskEstimationChanged(c.ProjectTaskId, c.ProjectId, c.ProjectTaskEstimation);
                ProcessEvent(projectTaskEstimationChanged);
            });

            Command<Commands.V1.ChangeProjectTaskType>(c =>
            {
                var projectTaskTypeChanged = new Events.V1.ProjectTaskTypeChanged(c.ProjectTaskId, c.ProjectId, c.ProjectTaskType);
                ProcessEvent(projectTaskTypeChanged);
            });

            Command<Commands.V1.AssignProjectTask>(c =>
            {
                var projectTaskAssigned = new Events.V1.ProjectTaskAssigned(c.ProjectTaskId, c.ProjectId, c.AssignUserId);
                ProcessEvent(projectTaskAssigned);
            });

            Command<Commands.V1.StartProjectTasKWork>(c =>
            {
                var projectTaskWorkStarted = new Events.V1.ProjectTaskWorkStarted(c.ProjectTaskId, c.ProjectId);
                ProcessEvent(projectTaskWorkStarted);
            });

            Command<Commands.V1.CompleteProjectTask>(c =>
            {
                var projectTaskCompleted = new Events.V1.ProjectTaskCompleted(c.ProjectTaskId, c.ProjectId);
                ProcessEvent(projectTaskCompleted);
            });
        }

        private void ProcessEvent(IProjectTaskEvent @event)
        {
            Persist(@event, evt =>
            {
                ProjectTask.Apply(@event);
                if (LastSequenceNr % SnapshotInterval == 0)
                {
                    QueryOffset = LastSequenceNr;
                    var projectTaskSnapshot = new ProjectTaskSnapshot(
                        ProjectTask.Id,
                        ProjectTask.ProjectId ?? Guid.Empty,
                        ProjectTask.TaskTitle ?? "",
                        ProjectTask.TaskDescription ?? "",
                        (int)ProjectTask.TaskType,
                        ProjectTask.TaskEstimation?.Value,
                        (int)ProjectTask.TaskStatus,
                        ProjectTask.AssignedUserId ?? Guid.Empty,
                        QueryOffset);
                    SaveSnapshot(projectTaskSnapshot);

                    DeleteSnapshots(new SnapshotSelectionCriteria(LastSequenceNr - 1));
                }
                _mediator.Tell(new Publish(_projectTaskTopic, @event));
                _log.Info($"{@event.ToString()} for {PersistenceId} has been saved");
            });
        }
    }
}