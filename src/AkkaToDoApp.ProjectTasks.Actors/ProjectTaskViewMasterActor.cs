﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;
using static AkkaToDoApp.ProjectTasks.Commands;
using static AkkaToDoApp.ProjectTasks.QueryModels;

namespace AkkaToDoApp.ProjectTasks.Actors
{
    public class ProjectTaskViewMasterActor : ReceiveActor, IWithUnboundedStash
    {
        private ILoggingAdapter _log = Context.GetLogger();
        IPersistenceIdsQuery _persistenceIdsQuery;
        private readonly IActorRef _userViewActor;
        private readonly IActorRef _projectViewActor;
        private IActorRef _shardRegion;
        public IStash Stash { get; set; }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(
                10,
                TimeSpan.FromSeconds(10),
                ex =>
                {
                    switch (ex)
                    {
                        case ArgumentException ae:
                            return Directive.Restart;
                        default:
                            return Directive.Escalate;
                    }
                });
        }

        public record BeginTrackProjectTasks
        {
            public IActorRef ShardRegion { get; }

            public BeginTrackProjectTasks(IActorRef shardRegion)
            {
                ShardRegion = shardRegion;
            }
        }

        public ProjectTaskViewMasterActor(
            IPersistenceIdsQuery persistenceIdsQuery,
            IActorRef userViewActor,
            IActorRef projectViewActor)
        {
            _persistenceIdsQuery = persistenceIdsQuery;
            _userViewActor = userViewActor;
            _projectViewActor = projectViewActor;

            WaitingForSharding();
        }

        protected override void PreStart()
        {
            _log.Info("Starting project task view master actor...");

            var mat = Context.Materializer();
            var self = Self;

            _persistenceIdsQuery
                .PersistenceIds()
                .Where(x => x.EndsWith(EntityIdHepler.ProjectTaskSuffix))
                .Select(x => new Ping(Guid.Parse(EntityIdHepler.ExtractEntityIdFormPersistenceId(x))))
                .RunWith(Sink.ActorRef<Ping>(Self, UnexpectedEndOfSream.Instance), mat);
        }

        protected override void PostStop()
        {
            _log.Info("Project task view master actor has been stopped");
        }

        private void WaitingForSharding()
        {
            Receive<BeginTrackProjectTasks>(b =>
            {
                var mediator = DistributedPubSub.Get(Context.System).Mediator;
                _log.Info("Received access to project task mediator. Starting project views...");
                _shardRegion = b.ShardRegion;
                Become(Running);
            });

            ReceiveAny(_ => Stash.Stash());
        }

        private void Running()
        {
            Receive<Ping>(p =>
            {
                IActorRef child = GetChild(p.ProjectTaskId);
                child.Forward(p);
            });

            Receive<GetProjectTaskHistoryById>(q =>
            {
                IActorRef child = GetChild(q.ProjectTaskId);
                child.Forward(q);
            });

            Receive<GetProjectTaskProjectionById>(q =>
            {
                IActorRef child = GetChild(q.ProjectTaskId);
                child.Forward(q);
            });
        }

        private IActorRef GetChild(Guid projectTaskId)
        {
            string childName = projectTaskId + EntityIdHepler.ProjectTaskSuffix;
            IActorRef mediator = DistributedPubSub.Get(Context.System).Mediator;
            IActorRef child = Context.Child(childName).GetOrElse(() => Context.ActorOf(Props.Create(() => new ProjectTaskViewActor(projectTaskId, _userViewActor, _projectViewActor, _shardRegion, mediator)), childName));
            Context.Watch(child);
            return child;
        }
    }
}
