﻿using AkkaToDoApp.Common.Domain;

namespace AkkaToDoApp.Projects.Domain
{
    public record ProjectId : AggregateId
    {
        ProjectId(Guid value) : base(value) { }

        public static ProjectId FromGuid(Guid value) => new ProjectId(value);
    }
}
