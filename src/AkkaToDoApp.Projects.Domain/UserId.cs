﻿namespace AkkaToDoApp.Projects.Domain
{
    public record UserId
    {
        public Guid Value { get; }

        public UserId() { }

        protected UserId(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentException("Value is empty");

            Guid result;

            if (!Guid.TryParse(value, out result))
                throw new ArgumentException("Value format is incorrect");

            if (result == Guid.Empty)
                throw new ArgumentException("Guid value is empty");

            Value = result;
        }

        protected UserId(Guid value)
        {
            if (value == Guid.Empty)
                throw new ArgumentException("Guid value is empty");

            Value = value;
        }

        public static implicit operator Guid(UserId userId) => userId.Value;

        public static UserId FromString(string value) => new UserId(value);

        public static UserId FromGuid(Guid value) => new UserId(value);

        public override string ToString() => Value.ToString();
    }
}
