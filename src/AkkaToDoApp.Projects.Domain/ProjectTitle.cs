﻿using static System.String;

namespace AkkaToDoApp.Projects.Domain
{
    public record ProjectTitle
    {
        public string Value { get; }

        internal ProjectTitle(string value) => Value = value;

        public static ProjectTitle FromString(string value)
        {
            CheckValidity(value);
            return new ProjectTitle(value);
        }

        public static implicit operator string(ProjectTitle value) => value.Value;

        private static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new ArgumentException("Title cannot be empty");

            if (value.Length < 10)
                throw new ArgumentException("Title cannot be shorter than 10 characters");

            if (value.Length > 100)
                throw new ArgumentException("Title cannot be longer than 100 characters");
        }
    }
}
