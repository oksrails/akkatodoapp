﻿using static System.String;

namespace AkkaToDoApp.Projects.Domain
{
    public record ProjectDescription
    {
        public string Value { get; }

        protected ProjectDescription(string value) => Value = value;

        public static ProjectDescription FromString(string value)
        {
            CheckValidity(value);
            return new ProjectDescription(value);
        }

        public static implicit operator string(ProjectDescription value) => value.Value;

        private static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new ArgumentException("Description cannot be empty");

            if (value.Length < 10)
                throw new ArgumentException("Description cannot be shorter than 10 characters");
        }
    }
}
