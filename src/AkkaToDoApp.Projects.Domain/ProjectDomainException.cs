﻿namespace AkkaToDoApp.Projects.Domain
{
    public class ProjectDomainException : Exception
    {
        public ProjectDomainException(string message)
            : base(message) { }
    }
}
