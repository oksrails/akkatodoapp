﻿namespace AkkaToDoApp.Projects.Domain
{
    public record Events
    {
        public record V1
        {
            public record ProjectCreated : IProjectEvent
            {
                public Guid ProjectId { get; init; }
                public string ProjectTitle { get; init; }
                public string ProjectDescription { get; init; }
                public Guid ProjectOwnerId { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectCreated(
                    Guid id,
                    string projectTitle,
                    string projectDescription,
                    Guid projectOwnerId)
                {
                    ProjectId = id;
                    ProjectTitle = projectTitle;
                    ProjectDescription = projectDescription;
                    ProjectOwnerId = projectOwnerId;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectCreated)}";
            }

            public record ProjectTitleChanged : IProjectEvent
            {
                public Guid ProjectId { get; init; }
                public string ProjectTitle { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTitleChanged(
                    Guid id,
                    string projectTitle)
                {
                    ProjectId = id;
                    ProjectTitle = projectTitle;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTitleChanged)}";
            }

            public record ProjectDescriptionChanged : IProjectEvent
            {
                public Guid ProjectId { get; init; }
                public string ProjectDescription { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectDescriptionChanged(
                    Guid id,
                    string projectDescription)
                {
                    ProjectId = id;
                    ProjectDescription = projectDescription;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectDescriptionChanged)}";
            }
        }
    }
}