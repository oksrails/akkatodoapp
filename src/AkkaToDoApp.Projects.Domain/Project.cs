﻿using AkkaToDoApp.Common.Domain;
using static AkkaToDoApp.Projects.Domain.Events;

namespace AkkaToDoApp.Projects.Domain
{
    public class Project : AggregateRoot
    {
        public ProjectTitle? ProjectTitle { get; private set; }
        public ProjectDescription? ProjectDescription { get; private set; }
        public UserId? ProjectOwnerId { get; private set; }

        public Project() { }

        public Project(
            Guid projectId,
            Guid projectOwnerId)
        {
            Id = projectId;
            ProjectOwnerId = UserId.FromGuid(projectOwnerId);
        }

        public Project(
            Guid projectId,
            Guid projectOwnerId,
            string projectTitle,
            string projectDescription)
        {
            Id = projectId;
            ProjectOwnerId = UserId.FromGuid(projectOwnerId);
            ProjectTitle = ProjectTitle.FromString(projectTitle);
            ProjectDescription = ProjectDescription.FromString(projectDescription);
        }

        protected override void EnsureValidState()
        {
            
        }

        protected override void When(object evt)
        {
            switch (evt)
            {
                case V1.ProjectCreated e:
                    Id = e.ProjectId;
                    ProjectTitle = ProjectTitle.FromString(e.ProjectTitle);
                    ProjectDescription = ProjectDescription.FromString(e.ProjectDescription);
                    ProjectOwnerId = UserId.FromGuid(e.ProjectOwnerId);
                    break;
                case V1.ProjectTitleChanged e:
                    ProjectTitle = ProjectTitle.FromString(e.ProjectTitle);
                    break;
                case V1.ProjectDescriptionChanged e:
                    ProjectDescription = ProjectDescription.FromString(e.ProjectDescription);
                    break;
                default:
                    throw new ProjectDomainException($"Project domain event {nameof(evt)} does not recognized");
            }
        }
    }
}