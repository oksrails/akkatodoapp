﻿namespace AkkaToDoApp.Projects.Domain
{
    public interface IProjectEvent
    {
        Guid ProjectId { get; init; }
        DateTime CreatedAt { get; init; }
    }
}
