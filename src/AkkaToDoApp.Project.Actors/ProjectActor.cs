﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence;
using AkkaToDoApp.Common.Actors;
using AkkaToDoApp.Common.Subscriptions;
using AkkaToDoApp.Projects.Domain;
using AkkaToDoApp.Projects.Dto;

namespace AkkaToDoApp.Projects.Actors
{
    public class ProjectActor : ReceivePersistentActor
    {
        ILoggingAdapter _log = Context.GetLogger();
        private const int SnapshotInterval = 10;
        public override string PersistenceId { get; }
        public Guid ProjectId { get; }
        public Project _project { get; set; }
        public long QueryOffset { get; private set; }
        private IActorRef _mediator;
        private readonly string _projectTopic;
        public ProjectActor(string persistenceId, IActorRef mediator)
        {
            PersistenceId = persistenceId;
            ProjectId = Guid.Parse(EntityIdHepler.ExtractEntityIdFormPersistenceId(persistenceId));
            _project = new Project();
            _projectTopic = TopicHelper.ProjectTopic(ProjectId);
            _mediator = mediator;

            Recovers();
            Commands();
        }

        protected override void PreStart()
        {
            _log.Info($"Project actor {PersistenceId} has been started");
            base.PreStart();
        }

        protected override void PostStop()
        {
            _log.Info($"Project actor {PersistenceId} has been stopped");
            base.PostStop();
        }

        private void Recovers()
        {
            Recover<SnapshotOffer>(offer =>
            {
                if (offer.Snapshot is ProjectSnapshot projectSnapshot)
                {
                    _project = new Project(
                        projectSnapshot.ProjectId,
                        projectSnapshot.ProjectOwner,
                        projectSnapshot.ProjectTitle ?? "",
                        projectSnapshot.ProjectDescription ?? "");
                    QueryOffset = projectSnapshot.QueryOffset;
                }
            });

            Recover<IProjectEvent>(e =>
            {
                _project.Apply(e);
            });
        }

        private void Commands()
        {
            Command<Commands.V1.CreateProject>(c =>
            {
                var projectCreated = new Events.V1.ProjectCreated(c.ProjectId, c.ProjectTitle, c.ProjectDescription, c.ProjectOwnerId);
                ProcessEvent(projectCreated);
            });

            Command<Commands.V1.ChangeProjectTitle>(c =>
            {
                var projectTitleChanged = new Events.V1.ProjectTitleChanged(c.ProjectId, c.ProjectTitle);
                ProcessEvent(projectTitleChanged);
            });

            Command<Commands.V1.ChangeProjectDescription>(c =>
            { 
                var projectDescriptionChanged = new Events.V1.ProjectDescriptionChanged(c.ProjectId, c.ProjectDescription);
                ProcessEvent(projectDescriptionChanged);
            });
        }

        private void ProcessEvent(IProjectEvent @event)
        {
            Persist(@event, evt =>
            {
                _project.Apply(@event);
                QueryOffset = LastSequenceNr;
                if (LastSequenceNr % SnapshotInterval == 0)
                {
                    var projectSnapshot = new ProjectSnapshot(
                        _project.Id,
                        _project.ProjectTitle ?? "",
                        _project.ProjectDescription ?? "",
                        _project.ProjectOwnerId ?? Guid.Empty,
                        QueryOffset);
                    SaveSnapshot(projectSnapshot);
                    DeleteSnapshots(new SnapshotSelectionCriteria(LastSequenceNr - 1));
                }
            });
            _mediator.Tell(new Publish(_projectTopic, @event));
            _log.Info($"{@event.ToString()} for {PersistenceId} has been saved");
        }
    }
}
