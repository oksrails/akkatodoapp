﻿using Akka.Actor;
using Akka.Event;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;
using static AkkaToDoApp.Projects.Commands;

namespace AkkaToDoApp.Projects.Actors
{
    public class ProjectInitiatorActor : ReceiveActor
    {
        private readonly ILoggingAdapter _log = Context.GetLogger();
        private IPersistenceIdsQuery _persistenceIdsQuery;
        private readonly IActorRef _projectQueryProxy;
        private ICancelable _heartbeatInterval;
        private readonly HashSet<Guid> _persistenceIds = new HashSet<Guid>();

        private record HeartBeat
        {
            private HeartBeat() { }

            public static readonly HeartBeat Instence = new HeartBeat();
        }

        public ProjectInitiatorActor(IPersistenceIdsQuery persistenceIdsQuery, IActorRef projectQueryProxy)
        {
            _persistenceIdsQuery = persistenceIdsQuery;
            _projectQueryProxy = projectQueryProxy;

            Process();
        }

        private void Process()
        {
            Receive<Ping>(p =>
            {
                _persistenceIds.Add(p.ProjectId);
                _projectQueryProxy.Tell(p);
            });

            Receive<HeartBeat>(h =>
            {
                foreach (var id in _persistenceIds)
                {
                    _projectQueryProxy.Tell(new Ping(id));
                }
            });

            Receive<UnexpectedEndOfSream>(e =>
            {
                _log.Warning("Received unexpected end of PersistenceIds stream. Restarting...");
                throw new ApplicationException("Restart");
            });
        }

        protected override void PreStart()
        {
            var mat = Context.Materializer();
            _persistenceIdsQuery.PersistenceIds()
                .Where(x => x.EndsWith(EntityIdHepler.ProjectSuffix))
                .Select(x => new Ping(Guid.Parse(EntityIdHepler.ExtractEntityIdFormPersistenceId(x))))
                .RunWith(Sink.ActorRef<Ping>(Self, UnexpectedEndOfSream.Instance), mat);

            _heartbeatInterval = Context.System.Scheduler.ScheduleTellRepeatedlyCancelable(
                TimeSpan.FromSeconds(30),
                TimeSpan.FromSeconds(30),
                Self,
                HeartBeat.Instence,
                ActorRefs.NoSender);
        }

        protected override void PostStop()
        {
            _heartbeatInterval?.Cancel();
        }
    }
}
