﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;

namespace AkkaToDoApp.Projects.Actors
{
    public class ProjectMasterActor : ReceiveActor
    {
        ILoggingAdapter _log = Context.GetLogger();

        public ProjectMasterActor()
        {
            Receive<IProjectCommand>(s =>
            {
                var projectActor = Context.Child($"{s.ProjectId.ToString()}-project").GetOrElse(() => StartChild(s.ProjectId));
                projectActor.Forward(s);
            });
        }

        protected override void PreStart()
        {
            _log.Info($"Starting project master actor...");
            base.PreStart();
        }

        protected override void PostStop()
        {
            _log.Info($"Project master actor has been stopped");
            base.PostStop();
        }

        private IActorRef StartChild(Guid projectId)
        {
            var persistenceId = $"{projectId.ToString()}-project";
            var mediator = DistributedPubSub.Get(Context.System).Mediator;
            return Context.ActorOf(Props.Create(() => new ProjectActor(persistenceId, mediator)), persistenceId);
        }
    }
}