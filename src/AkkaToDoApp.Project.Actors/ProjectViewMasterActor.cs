﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;
using System.Collections.Immutable;
using static AkkaToDoApp.Projects.QueryModels;
using static AkkaToDoApp.Common.Actors.Messages;

namespace AkkaToDoApp.Projects.Actors
{
    public class ProjectViewMasterActor : ReceiveActor, IWithUnboundedStash
    {
        private ILoggingAdapter _log = Context.GetLogger();
        ICurrentPersistenceIdsQuery _persistenceIdsQuery;
        private readonly IActorRef _userViewActor;
        private IActorRef _shardRegion { get; set; }
        public IStash Stash { get; set; }

        public record BeginTrackProjects
        {
            public IActorRef ShardRegion { get; }

            public BeginTrackProjects(IActorRef shardRegion)
            {
                ShardRegion = shardRegion;
            }
        }

        public ProjectViewMasterActor(
            ICurrentPersistenceIdsQuery persistenceIdsQuery,
            IActorRef userViewActor)
        {
            _persistenceIdsQuery = persistenceIdsQuery;
            _userViewActor = userViewActor;
            WaitingForSharding();
        }

        protected override void PreStart()
        {
            _log.Info("Starting project view master actor...");
        }

        protected override void PostStop()
        {
            _log.Info("Project view master actor has been stopped");
        }

        private void WaitingForSharding()
        {
            Receive<BeginTrackProjects>(b =>
            {
                _log.Info("Received access to project mediator... Starting project views...");
                var mediator = DistributedPubSub.Get(Context.System).Mediator;
                _shardRegion = b.ShardRegion;
                var projectIds = GetProjectIds();
                foreach(var projectId in projectIds)
                {
                    var childName = projectId + EntityIdHepler.ProjectSuffix;
                    Context.ActorOf(Props.Create(() => new ProjectViewActor(projectId, _shardRegion, _userViewActor, mediator)), childName);
                }

                Become(Running);
                Stash.UnstashAll();
            });

            ReceiveAny(_ => Stash.Stash());
        }

        private void Running()
        {
            Receive<Request.CheckRunningRequest>(r =>
            {
                Sender.Tell(new Response.CheckRunningResponse(true));
            });

            Receive<GetProjectHistoryById>(q =>
            {
                IActorRef child = GetChild(q.ProjectId);
                child.Forward(q);
            });

            Receive<GetProjectProjectionById>(q =>
            {
                IActorRef child = GetChild(q.ProjectId);
                child.Forward(q);
            });
        }

        private ImmutableHashSet<Guid> GetProjectIds()
        {
            ActorMaterializer mat = Context.Materializer();
            ImmutableHashSet<Guid> projectIds = _persistenceIdsQuery
                .CurrentPersistenceIds()
                .Where(p => p.EndsWith(EntityIdHepler.ProjectSuffix))
                .RunAggregate(
                    ImmutableHashSet<Guid>.Empty,
                    (acc, c) => acc.Add(Guid.Parse(EntityIdHepler.ExtractEntityIdFormPersistenceId(c))),
                    mat)
                .Result;
            return projectIds;
        }

        private IActorRef GetChild(Guid projectId)
        {
            string childName = projectId + EntityIdHepler.ProjectSuffix;
            IActorRef child = Context.Child(childName);
            if (child.IsNobody())
            {
                ActorMaterializer mat = Context.Materializer();
                ImmutableHashSet<Guid> projectIds = _persistenceIdsQuery
                    .CurrentPersistenceIds()
                    .Where(p => p.Equals(projectId + EntityIdHepler.ProjectSuffix))
                    .RunAggregate(
                        ImmutableHashSet<Guid>.Empty,
                        (acc, c) => acc.Add(Guid.Parse(EntityIdHepler.ExtractEntityIdFormPersistenceId(c))),
                        mat)
                    .Result;
                if (projectIds.Any())
                {
                    IActorRef mediator = DistributedPubSub.Get(Context.System).Mediator;
                    child = Context.ActorOf(Props.Create(() => new ProjectViewActor(projectId, _shardRegion, _userViewActor, mediator)), childName);
                }
                else
                {
                    _log.Warning($"Message received for unknown project id: {projectId} - sending to dead letters");
                }
            }

            return child;
        }
    }
}
