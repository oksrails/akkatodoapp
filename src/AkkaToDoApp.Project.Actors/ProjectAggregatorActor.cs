﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;
using AkkaToDoApp.Common.Subscriptions;
using AkkaToDoApp.Projects.Domain;
using AkkaToDoApp.Projects.Dto;
using AkkaToDoApp.Projects.Subscriptions;
using AkkaToDoApp.Users.Domain;
using System.Collections.Immutable;
using static AkkaToDoApp.Projects.Commands;
using static AkkaToDoApp.Users.QueryModels;

namespace AkkaToDoApp.Projects.Actors
{
    public class ProjectAggregatorActor : ReceivePersistentActor, IWithUnboundedStash
    {
        public override string PersistenceId { get; }
        private Guid ProjectId { get; set; }
        public const int SnapshotEveryN = 10;
        private readonly IEventsByTagQuery _eventsByTag;
        private readonly ICurrentEventsByTagQuery _currentEventsByTag;
        private readonly IActorRef _mediator;
        private readonly IActorRef _userViewActor;
        private readonly ILoggingAdapter _log = Context.GetLogger();
        public IStash Stash { get; set; }
        public long QueryOffset { get; private set; }
        private ProjectHistory ProjectHistory;
        private Project Project;
        private ProjectProjection ProjectProjection;

        public ProjectAggregatorActor(
            string persistenceId,
            IEventsByTagQuery eventsByTag,
            ICurrentEventsByTagQuery currentEventsByTag,
            IActorRef userViewActor)
        {
            PersistenceId = persistenceId + EntityIdHepler.ProjectSuffix;
            ProjectId = Guid.Parse(persistenceId);
            _eventsByTag = eventsByTag;
            _currentEventsByTag = currentEventsByTag;
            _userViewActor = userViewActor;
            _mediator = DistributedPubSub.Get(Context.System).Mediator;
            ProjectHistory = new ProjectHistory(ProjectId, ImmutableHashSet<object>.Empty);
            Project = new Project();

            Recovers();
            Commands();
        }

        protected override void PreStart()
        {
            _log.Info($"Starting project aggregator for persistence {PersistenceId}...");
        }

        protected override void PostStop()
        {
            _log.Info($"Project aggregator for persistence {PersistenceId} has been stopped");
        }

        private void Recovers()
        {
            Recover<SnapshotOffer>(async s =>
            {
                if (s.Snapshot is ProjectSnapshot projectSnapshot)
                {
                    RecoverAggregatedData(projectSnapshot);
                    await GenerateProjectProjectionFromSnapshot(projectSnapshot);
                }    
            });

            Recover<ProjectSnapshot>(async s =>
            {
                RecoverAggregatedData(s);
                await GenerateProjectProjectionFromSnapshot(s);
            });

            Recover<IProjectEvent>(e =>
            {
                Project.Apply(e);
                ProjectHistory = ProjectHistory.AppendProjectEvent(e);
            });
        }

        private void Commands()
        {
            Command<EventEnvelope>(async e =>
            {
                _log.Debug($"Received project event {e.Event.ToString()}");
                if (e.Event is IProjectEvent projectEvent)
                {
                    ProjectHistory = ProjectHistory.AppendProjectEvent(projectEvent);
                    await GenerateProjectProjection();
                }
            });

            Command<PublishProjection>(p =>
            {
                _mediator.Tell(new Publish(TopicHelper.ProjectProjectionTopic(), ProjectProjection));
            });

            Command<FetchProjectHistory>(f =>
            {
                Sender.Tell(ProjectHistory);
            });

            Command<ProjectSubscribe>(async sub =>
            {
                _log.Info($"Received {sub} from {Sender}");
                await _mediator.Ask(new Subscribe(TopicHelper.ProjectProjectionTopic(), sub.Subscriber));
                _mediator.Tell(new Publish(TopicHelper.ProjectProjectionTopic(), ProjectProjection));
            });
        }

        protected override void OnReplaySuccess()
        {
            GenerateProjectProjection().Wait();

            var mat = Context.Materializer();
            var self = Self;

            _eventsByTag.EventsByTag(ProjectId.ToString(), Offset.NoOffset())
                .RunWith(Sink.ActorRef<EventEnvelope>(self, UnexpectedEndOfSream.Instance), mat);

            base.OnReplaySuccess();
        }

        private void InitializeProjectHistory()
        {
            var mat = Context.Materializer();
            var events = _currentEventsByTag
                .CurrentEventsByTag(ProjectId.ToString(), Offset.NoOffset())
                .Select(e => e.Event)
                .RunAggregate(
                    ImmutableHashSet<object>.Empty,
                    (acc, c) => acc.Add(c),
                    mat)
                .Result;

            ProjectHistory = new ProjectHistory(ProjectId, events);
        }

        private async Task GenerateProjectProjection()
        {
            string projectOwnerName = "";
            if (ProjectHistory.ProjectCurentState.ProjectOwnerId is not null)
            {
                var projectOwner = await GetUserProfile(new GetUserProfileByIdQuery { UserProfileId = ProjectHistory.ProjectCurentState.ProjectOwnerId });
                projectOwnerName = projectOwner.DisplayName ?? "";
            }
            ProjectProjection = new ProjectProjection(
                ProjectId,
                ProjectHistory.ProjectCurentState.ProjectTitle ?? "",
                ProjectHistory.ProjectCurentState.ProjectDescription ?? "",
                projectOwnerName);
            _mediator.Tell(new Publish(TopicHelper.ProjectProjectionTopic(), ProjectProjection));
        }

        private async Task GenerateProjectProjectionFromSnapshot(ProjectSnapshot projectSnapshot)
        {
            string projectOwnerName = "";
            if (ProjectHistory.ProjectCurentState.ProjectOwnerId is not null)
            {
                var projectOwner = await GetUserProfile(new GetUserProfileByIdQuery { UserProfileId = ProjectHistory.ProjectCurentState.ProjectOwnerId });
                projectOwnerName = projectOwner.DisplayName ?? "";
            }
            ProjectProjection = new ProjectProjection(
                ProjectId,
                projectSnapshot.ProjectTitle ?? "",
                projectSnapshot.ProjectDescription ?? "",
                projectOwnerName);
        }

        private async Task<UserProfile> GetUserProfile(GetUserProfileByIdQuery query)
        {
            var projectOwner = await _userViewActor.Ask(query);
            if (projectOwner is UserProfile userProfile)
            {
                return userProfile;
            }
            else
            {
                throw new ArgumentException($"Received unrecognized answer type from {_userViewActor.Path}", nameof(projectOwner));
            }
        }

        private void RecoverAggregatedData(ProjectSnapshot projectSnapshot)
        {
            Project = new Project(projectSnapshot.ProjectId, projectSnapshot.ProjectOwner, projectSnapshot.ProjectTitle ?? "", projectSnapshot.ProjectDescription ?? "");
            QueryOffset = projectSnapshot.QueryOffset;
        }
    }
}
