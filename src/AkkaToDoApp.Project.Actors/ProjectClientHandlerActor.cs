﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;
using AkkaToDoApp.Common.Subscriptions;
using AkkaToDoApp.Projects.Subscriptions;
using System.Collections.Immutable;
using static AkkaToDoApp.Projects.Commands;

namespace AkkaToDoApp.Projects.Actors
{
    public class ProjectClientHandlerActor : ReceiveActor
    {
        private ILoggingAdapter _log = Context.GetLogger();
        private IActorRef _shardRegion;
        private IActorRef _mediator;
        private ICurrentPersistenceIdsQuery _currentPersistenceIds;

        public ProjectClientHandlerActor(IActorRef shardRegion, ICurrentPersistenceIdsQuery currentPersistenceIds)
        {
            _shardRegion = shardRegion;
            _mediator = DistributedPubSub.Get(Context.System).Mediator;
            _currentPersistenceIds = currentPersistenceIds;

            ReceiveAsync<SubscribeProjectClient>(async s =>
            {
                _log.Info($"Received {s} from {Sender}");
                await _mediator.Ask(new Subscribe(TopicHelper.ProjectProjectionTopic(), s.Subscriber));
                Context.Watch(s.Subscriber);
                SendPublishMessage();
            });

            ReceiveAsync<Terminated>(async t =>
            {
                await _mediator.Ask(new Unsubscribe(TopicHelper.ProjectProjectionTopic(), Sender));
            });
        }

        protected override void PreStart()
        {
            _log.Info($"Starting project cluster client handler actor...");
        }

        protected override void PostStop()
        {
            _log.Info($"Project cluster client handler actor stopped");
        }

        private void SendPublishMessage()
        {
            ActorMaterializer mat = Context.Materializer();
            var projectIds = _currentPersistenceIds
                .CurrentPersistenceIds()
                .Where(p => p.EndsWith(EntityIdHepler.ProjectSuffix))
                .RunAggregate(
                    ImmutableHashSet<Guid>.Empty,
                    (acc, c) => acc.Add(Guid.Parse(EntityIdHepler.ExtractEntityIdFormPersistenceId(c))),
                    mat)
                .Result;

            foreach (var projectId in projectIds)
            {
                _shardRegion.Tell(new PublishProjection(projectId));
            }
        }
    }
}
