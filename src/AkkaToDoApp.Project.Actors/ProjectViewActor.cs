﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence.Query;
using AkkaToDoApp.Common.Subscriptions;
using AkkaToDoApp.Projects.Domain;
using AkkaToDoApp.Projects.Dto;
using AkkaToDoApp.Users.Domain;
using static AkkaToDoApp.Projects.Commands;
using static AkkaToDoApp.Projects.QueryModels;
using static AkkaToDoApp.Users.QueryModels;

namespace AkkaToDoApp.Projects.Actors
{
    public class ProjectViewActor : ReceiveActor, IWithUnboundedStash
    {
        ILoggingAdapter _log = Context.GetLogger();
        public IStash Stash { get; set; }
        private readonly IActorRef _userViewActor;
        private readonly IActorRef _projectActorGateway;
        private readonly IActorRef _mediator;
        private IActorRef _tickerEntity;
        public Guid ProjectId { get; }
        private ProjectProjection ProjectProjection { get; set; }
        private ProjectHistory ProjectHistory { get; set; }
        private long QueryOffset = 0;
        private readonly string _projectTopic;
        public ProjectViewActor(Guid projectId, IActorRef projectActorGateway, IActorRef userViewActor, IActorRef mediator)
        {
            ProjectId = projectId;
            _projectTopic = TopicHelper.ProjectTopic(ProjectId);
            _projectActorGateway = projectActorGateway;
            _mediator = mediator;
            _userViewActor = userViewActor;

            WaitingForVolume();
        }

        protected override async void PreStart()
        {
            _log.Info("Starting project view actor...");

            Context.SetReceiveTimeout(TimeSpan.FromSeconds(5));
            _projectActorGateway.Tell(new FetchProjectHistory(ProjectId));
        }

        protected override void PostStop()
        {
            _log.Info("Project view actor has been stopped");
        }

        private void WaitingForVolume()
        {
            Receive<ProjectSnapshot>(s =>
            {
                GenerateProjectProjectionFromSnapshot(s);
                _tickerEntity = Sender;
                _mediator.Tell(new Subscribe(_projectTopic, Self));
            });

            Receive<ProjectHistory>(h =>
            {
                ProjectHistory = h;
                GenerateProjectProjection().Wait();
                _tickerEntity = Sender;
                _mediator.Tell(new Subscribe(_projectTopic, Self));
            });

            Receive<SubscribeAck>(ack =>
            {
                _log.Info($"Subscribed to {_projectTopic} - ready for real-time processing.");
                Become(Processing);
                Stash.UnstashAll();
                Context.Watch(_tickerEntity);
                Context.SetReceiveTimeout(null);
            });

            Receive<ReceiveTimeout>(timeout =>
            {
                _log.Warning($"Received no initial values for project id: {ProjectId} from source of truth after 5s. Retrying...");
                _projectActorGateway.Tell(new FetchProjectHistory(ProjectId));
            });

            ReceiveAny(_ => Stash.Stash());
        }

        private void Processing()
        {
            Receive<IProjectEvent>(async e =>
            {
                ProjectHistory = ProjectHistory.AppendProjectEvent(e);
                await GenerateProjectProjection();
            });

            Receive<EventEnvelope>(async e =>
            {
                if (e.Event is IProjectEvent projectEvent)
                {
                    ProjectHistory = ProjectHistory.AppendProjectEvent(projectEvent);
                    await GenerateProjectProjection();

                    if (e.Offset is Sequence s)
                    {
                        QueryOffset = s.Value;
                    }
                }
            });

            Receive<GetProjectHistoryById>(q =>
            {
                if (q.ProjectId.Equals(ProjectId))
                {
                    Sender.Tell(ProjectHistory);
                }
                else
                {
                    _log.Warning($"Expected project id: {ProjectId}, but received {q.ProjectId}");
                }
            });

            Receive<GetProjectProjectionById>(q =>
            {
                if (q.ProjectId.Equals(ProjectId))
                {
                    Sender.Tell(ProjectProjection);
                }
                else
                {
                    _log.Warning($"Expected project id: {ProjectId}, but received {q.ProjectId}");
                }
            });

            Receive<Terminated>(t =>
            {
                if (t.ActorRef.Equals(Self))
                {
                    _log.Info("Source of truth entity terminated. Re-acquiring...");
                    Context.SetReceiveTimeout(TimeSpan.FromSeconds(5));
                    _projectActorGateway.Tell(new FetchProjectHistory(ProjectId));
                    _mediator.Tell(new Unsubscribe(_projectTopic, Self));
                    Become(WaitingForVolume);
                }
            });
        }

        private async Task GenerateProjectProjection()
        {
            var projectOwner = await GetUserProfile(new GetUserProfileByIdQuery { UserProfileId = ProjectHistory.ProjectCurentState.ProjectOwnerId });
            ProjectProjection = new ProjectProjection(
                ProjectId,
                ProjectHistory.ProjectCurentState.ProjectTitle,
                ProjectHistory.ProjectCurentState.ProjectDescription,
                projectOwner.DisplayName);
            _mediator.Tell(new Publish(TopicHelper.ProjectProjectionTopic(), ProjectProjection));
        }

        private async void GenerateProjectProjectionFromSnapshot(ProjectSnapshot projectSnapshot)
        {
            var projectOwner = await GetUserProfile(new GetUserProfileByIdQuery { UserProfileId = ProjectHistory.ProjectCurentState.ProjectOwnerId});
            ProjectProjection = new ProjectProjection(
                ProjectId,
                projectSnapshot.ProjectTitle,
                projectSnapshot.ProjectDescription,
                projectOwner.DisplayName);
        }

        private async Task<UserProfile> GetUserProfile(GetUserProfileByIdQuery query)
        {
            var projectOwner = await _userViewActor.Ask(query);
            if (projectOwner is UserProfile userProfile)
            {
                return userProfile;
            }
            else
            {
                throw new ArgumentException($"Received unrecognized answer type from {_userViewActor.Path}", nameof(projectOwner));
            }
        }
    }
}
