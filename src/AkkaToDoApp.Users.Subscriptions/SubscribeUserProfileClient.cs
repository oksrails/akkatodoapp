﻿using Akka.Actor;

namespace AkkaToDoApp.Users.Subscriptions
{
    public class SubscribeUserProfileClient
    {
        public IActorRef Subscriber { get; init; }
        
        public SubscribeUserProfileClient(IActorRef subscriber)
        {
            Subscriber = subscriber;
        }
    }
}