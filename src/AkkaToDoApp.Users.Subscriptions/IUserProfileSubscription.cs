﻿namespace AkkaToDoApp.Users.Subscriptions
{
    public interface IUserProfileSubscription
    {
        Guid UserProfileId { get; init; }
    }
}
