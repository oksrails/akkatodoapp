﻿using Akka.Actor;

namespace AkkaToDoApp.Users.Subscriptions
{
    public class UserProfileSubscribe : IUserProfileSubscription
    {
        public Guid UserProfileId { get; init; }
        public IActorRef Suscriber;

        public UserProfileSubscribe(Guid userProfileId, IActorRef subscriber)
        {
            UserProfileId = userProfileId;
            Suscriber = subscriber;
        }
    }
}