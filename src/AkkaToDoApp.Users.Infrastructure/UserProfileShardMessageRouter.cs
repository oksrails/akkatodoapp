﻿using Akka.Cluster.Sharding;
using AkkaToDoApp.Users.Domain;
using AkkaToDoApp.Users.Subscriptions;

namespace AkkaToDoApp.Users.Infrastructure
{
    public class UserProfileShardMessageRouter : HashCodeMessageExtractor
    {
        public const int DefaultNumberOfShards = 30;

        public UserProfileShardMessageRouter(int maxNumberOfShards) : base(maxNumberOfShards) { }

        public UserProfileShardMessageRouter() : this(DefaultNumberOfShards) { }

        public override string EntityId(object message)
        {
            switch (message)
            {
                case IUserProfileEvent userEvent:
                    return userEvent.UserProfileId.ToString();
                case IUserProfileCommand userCommand:
                    return userCommand.UserProfileId.ToString();
                case IUserProfileSubscription userSubscription:
                    return userSubscription.UserProfileId.ToString();
                case ShardRegion.StartEntity se:
                    return se.EntityId;
                case ShardingEnvelope e:
                    return e.EntityId;
                default: 
                    return string.Empty;
            }
        }

        public override object EntityMessage(object message)
        {
            return message;
        }

        public override string ShardId(object message)
        {
            return EntityId(message);
        }
    }
}