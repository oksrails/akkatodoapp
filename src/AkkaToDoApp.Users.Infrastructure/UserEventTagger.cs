﻿using Akka.Persistence.Journal;
using System.Collections.Immutable;
using static AkkaToDoApp.Users.Domain.Events;

namespace AkkaToDoApp.Users.Infrastructure
{
    public class UserEventTagger : IWriteEventAdapter
    {
        public string Manifest(object evt)
        {
            return string.Empty;
        }

        public object ToJournal(object evt)
        {
            switch (evt)
            {
                case V1.UserCreated userCreated:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(userCreated.UserProfileId.ToString()).Add(userCreated.ToString()));
                case V1.UserFirstNameChanged userFirstNameChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(userFirstNameChanged.UserProfileId.ToString()).Add(userFirstNameChanged.ToString()));
                case V1.UserLastNameChanged userLastNameChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(userLastNameChanged.UserProfileId.ToString()).Add(userLastNameChanged.ToString()));
                case V1.UserDisplayNameChanged userDisplayNameChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(userDisplayNameChanged.UserProfileId.ToString()).Add(userDisplayNameChanged.ToString()));
                default:
                    return evt;
            }
        }
    }
}
