﻿namespace AkkaToDoApp.ProjectTasks.Domain
{
    public class ProjectTaskDomainException : Exception
    {
        public ProjectTaskDomainException(string message) : base(message) { }
    }
}
