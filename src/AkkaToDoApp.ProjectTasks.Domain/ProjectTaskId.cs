﻿using AkkaToDoApp.Common.Domain;

namespace AkkaToDoApp.ProjectTasks.Domain
{
    public record ProjectTaskId : AggregateId
    {
        ProjectTaskId(Guid value) : base(value) { }

        public static ProjectTaskId FromGuid(Guid value) => new ProjectTaskId(value);
    }
}
