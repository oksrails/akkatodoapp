﻿using static System.String;

namespace AkkaToDoApp.ProjectTasks.Domain
{
    public record TaskDescription
    {
        public string Value { get; }

        internal TaskDescription(string value) => Value = value;

        public static TaskDescription FromString(string value)
        {
            CheckValidity(value);
            return new TaskDescription(value);
        }

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new ProjectTaskDomainException("Task description cannot be empty");

            if (value.Length < 10)
                throw new ProjectTaskDomainException("Task description cannot be shorter then 10 characters");
        }

        public static implicit operator string(TaskDescription projectDescription) => projectDescription.Value;
    }
}
