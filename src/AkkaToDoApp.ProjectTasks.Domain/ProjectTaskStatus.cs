﻿namespace AkkaToDoApp.ProjectTasks.Domain
{
    public enum ProjectTaskStatus
    {
        Open, InProgress, Done
    }
}
