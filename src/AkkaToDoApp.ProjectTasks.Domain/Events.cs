﻿namespace AkkaToDoApp.ProjectTasks.Domain
{
    public record Events
    {
        public record V1
        {
            public record ProjectTaskCreated : IProjectTaskEvent
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTaskCreated(
                    Guid id,
                    Guid projectId)
                {
                    ProjectTaskId = id;
                    ProjectId = projectId;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTaskCreated)}";
            }

            public record ProjectTaskTitleChanged : IProjectTaskEvent
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public string TaskTitle { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTaskTitleChanged(
                    Guid id,
                    Guid projectId,
                    string taskTitle)
                {
                    ProjectTaskId = id;
                    ProjectId = projectId;
                    TaskTitle = taskTitle;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTaskTitleChanged)}";
            }

            public record ProjectTaskDescriptionChanged : IProjectTaskEvent
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public string TaskDescription { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTaskDescriptionChanged(
                    Guid id,
                    Guid projectId,
                    string taskDescription)
                {
                    ProjectTaskId = id;
                    ProjectId = projectId;
                    TaskDescription = taskDescription;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTaskDescriptionChanged)}";
            }

            public record ProjectTaskTypeChanged : IProjectTaskEvent
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public int TaskType { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTaskTypeChanged(
                    Guid id,
                    Guid projectId,
                    int taskType)
                {
                    ProjectTaskId = id;
                    ProjectId = projectId;
                    TaskType = taskType;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTaskTypeChanged)}";
            }

            public record ProjectTaskEstimationChanged : IProjectTaskEvent
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public decimal TaskEstimation { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTaskEstimationChanged(
                    Guid id,
                    Guid projectId,
                    decimal taskEstimation)
                {
                    ProjectTaskId = id;
                    ProjectId = projectId;
                    TaskEstimation = taskEstimation;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTaskEstimationChanged)}";
            }

            public record ProjectTaskAssigned : IProjectTaskEvent
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public Guid AssignedUserId { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTaskAssigned(
                    Guid id,
                    Guid projectId,
                    Guid assignedUserId)
                {
                    ProjectTaskId = id;
                    ProjectId = projectId;
                    AssignedUserId = assignedUserId;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTaskAssigned)}";
            }

            public record ProjectTaskWorkStarted : IProjectTaskEvent
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTaskWorkStarted(
                    Guid id,
                    Guid projectId)
                {
                    ProjectTaskId = id;
                    ProjectId = projectId;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTaskWorkStarted)}";
            }

            public record ProjectTaskCompleted : IProjectTaskEvent
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public DateTime CreatedAt { get; init; }

                public ProjectTaskCompleted(
                    Guid id,
                    Guid projectId)
                {
                    ProjectTaskId = id;
                    ProjectId = projectId;
                    CreatedAt = DateTime.Now;
                }

                public override string ToString() => $"{nameof(ProjectTaskCompleted)}";
            }
        }
    }
}
