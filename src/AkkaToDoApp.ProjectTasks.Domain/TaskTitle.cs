﻿using static System.String;

namespace AkkaToDoApp.ProjectTasks.Domain
{
    public record TaskTitle
    {
        public string Value { get; }

        internal TaskTitle(string value) => Value = value;

        public static TaskTitle FromString(string value)
        {
            CheckValidity(value);
            return new TaskTitle(value);
        }

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new ProjectTaskDomainException("Task title cannot be empty");

            if (value.Length < 10)
                throw new ProjectTaskDomainException("Task title cannot be shorter then 10 characters");

            if (value.Length > 100)
                throw new ProjectTaskDomainException("Task title cannot be longer then 100 characters");
        }

        public static implicit operator string(TaskTitle taskTitle) => taskTitle.Value;
    }
}
