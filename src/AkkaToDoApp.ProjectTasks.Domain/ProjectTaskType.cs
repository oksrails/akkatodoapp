﻿namespace AkkaToDoApp.ProjectTasks.Domain
{
    public enum ProjectTaskType
    {
        Epic, Feature, UserStory
    }
}