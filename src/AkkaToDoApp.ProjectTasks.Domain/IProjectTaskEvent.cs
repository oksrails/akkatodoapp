﻿namespace AkkaToDoApp.ProjectTasks.Domain
{
    public interface IProjectTaskEvent
    {
        Guid ProjectTaskId { get; init; }
        DateTime CreatedAt { get; init; }
    }
}
