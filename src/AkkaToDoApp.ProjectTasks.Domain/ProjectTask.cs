﻿using AkkaToDoApp.Common.Domain;
using static AkkaToDoApp.ProjectTasks.Domain.Events;

namespace AkkaToDoApp.ProjectTasks.Domain
{
    public class ProjectTask : AggregateRoot
    {
        public ProjectId? ProjectId { get; set; }
        public TaskTitle? TaskTitle { get; set; }
        public TaskDescription? TaskDescription { get; set; }
        public ProjectTaskType TaskType { get; set; }
        public TaskEstimation? TaskEstimation { get; set; }
        public ProjectTaskStatus TaskStatus { get; private set; }
        public UserId? AssignedUserId { get; private set; }

        public ProjectTask() { }

        public ProjectTask(
            Guid projectTaskId,
            Guid projectId)
        {
            Id = projectTaskId;
            ProjectId = ProjectId.FromGuid(projectId);
            TaskStatus = ProjectTaskStatus.Open;
        }

        public ProjectTask(
            Guid projectTaskId,
            Guid projectId,
            string? taskTitle,
            string? taskDescription,
            int taskType,
            decimal taskEstimation,
            int taskStatus,
            Guid assignedUserId)
        {
            Id = projectTaskId;
            ProjectId = ProjectId.FromGuid(projectId);
            TaskTitle = !string.IsNullOrEmpty(taskTitle) ? TaskTitle.FromString(taskTitle) : null;
            TaskDescription = !string.IsNullOrEmpty(taskDescription) ? TaskDescription.FromString(taskDescription) : null;
            TaskType = (ProjectTaskType)taskType;
            TaskEstimation = TaskEstimation.FromDecimal(taskEstimation);
            TaskStatus = (ProjectTaskStatus)taskStatus;
            if (assignedUserId != Guid.Empty)
                AssignedUserId = UserId.FromGuid(assignedUserId);
        }

        protected override void EnsureValidState()
        {
            
        }

        protected override void When(object evt)
        {
            switch (evt)
            {
                case V1.ProjectTaskCreated e:
                    Id = e.ProjectTaskId;
                    ProjectId = ProjectId.FromGuid(e.ProjectId);
                    break;
                case V1.ProjectTaskTitleChanged e:
                    TaskTitle = TaskTitle.FromString(e.TaskTitle);
                    break;
                case V1.ProjectTaskDescriptionChanged e:
                    TaskDescription = TaskDescription.FromString(e.TaskDescription);
                    break;
                case V1.ProjectTaskTypeChanged e:
                    TaskType = (ProjectTaskType)e.TaskType;
                    break;
                case V1.ProjectTaskAssigned e:
                    AssignedUserId = UserId.FromGuid(e.AssignedUserId);
                    break;
                case V1.ProjectTaskEstimationChanged e:
                    TaskEstimation = TaskEstimation.FromDecimal(e.TaskEstimation);
                    break;
                case V1.ProjectTaskWorkStarted e:
                    TaskStatus = ProjectTaskStatus.InProgress;
                    break;
                case V1.ProjectTaskCompleted e:
                    TaskStatus = ProjectTaskStatus.Done;
                    break;
                default:
                    throw new ProjectTaskDomainException($"Project task domain event {nameof(evt)} does not recognized");
            }
        }
    }
}
