﻿namespace AkkaToDoApp.ProjectTasks.Domain
{
    public record ProjectId
    {
        public Guid Value { get; }

        public ProjectId() { }

        protected ProjectId(Guid value)
        {
            if (value == Guid.Empty)
                throw new ProjectTaskDomainException("Guid value is empty");

            Value = value;
        }

        public static ProjectId FromGuid(Guid value) => new ProjectId(value);

        public static implicit operator Guid(ProjectId value) => value.Value;

        public override string ToString() => Value.ToString();
    }
}
