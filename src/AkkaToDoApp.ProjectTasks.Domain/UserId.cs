﻿namespace AkkaToDoApp.ProjectTasks.Domain
{
    public record UserId
    {
        public Guid Value { get; }

        public UserId() { }

        protected UserId(Guid value) => Value = value;

        public static UserId FromGuid(Guid value) => new UserId(value);

        public static implicit operator Guid(UserId value) => value.Value;

        public override string ToString() => Value.ToString();
    }
}
