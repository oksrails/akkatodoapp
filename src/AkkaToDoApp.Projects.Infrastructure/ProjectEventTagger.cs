﻿using Akka.Persistence.Journal;
using System.Collections.Immutable;
using static AkkaToDoApp.Projects.Domain.Events;

namespace AkkaToDoApp.Projects.Infrastructure
{
    public class ProjectEventTagger : IWriteEventAdapter
    {
        public string Manifest(object evt)
        {
            return string.Empty;
        }

        public object ToJournal(object evt)
        {
            switch (evt)
            {
                case V1.ProjectCreated projectCreated:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectCreated.ProjectId.ToString()).Add(projectCreated.ToString()));
                case V1.ProjectTitleChanged projectTitleChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectTitleChanged.ProjectId.ToString()).Add(projectTitleChanged.ToString()));
                case V1.ProjectDescriptionChanged projectDescriptionChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectDescriptionChanged.ProjectId.ToString()).Add(projectDescriptionChanged.ToString()));
                default: 
                    return evt;
            }
        }
    }
}