﻿using Akka.Cluster.Sharding;
using AkkaToDoApp.Projects.Subscriptions;
using AkkaToDoApp.Projects.Domain;

namespace AkkaToDoApp.Projects.Infrastructure
{
    public class ProjectShardMessageRouter : HashCodeMessageExtractor, IMessageExtractor
    {
        public const int DefaultNumberOfShards = 30;

        public ProjectShardMessageRouter(int maxNumberOfShards) : base(maxNumberOfShards) { }

        public ProjectShardMessageRouter() : this(DefaultNumberOfShards) { }

        public override string EntityId(object message)
        {
            switch (message)
            {
                case IProjectEvent projectEvent:
                    return projectEvent.ProjectId.ToString();
                case IProjectCommand projectCommand:
                    return projectCommand.ProjectId.ToString();
                case IProjectIdQuery projectQuery:
                    return projectQuery.ProjectId.ToString();
                case IProjectSubscription projectSubscription:
                    return projectSubscription.ProjectId.ToString();
                case ShardRegion.StartEntity se:
                    return se.EntityId;
                case ShardingEnvelope e:
                    return e.EntityId;
                default: 
                    return string.Empty;
            }
        }

        public override object EntityMessage(object message)
        {
            return message;
        }

        public override string ShardId(object message)
        {
            return EntityId(message);
        }
    }
}
