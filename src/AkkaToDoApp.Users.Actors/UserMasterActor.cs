﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;

namespace AkkaToDoApp.Users.Actors
{
    /// <summary>
    /// Child-per entity parent for user profiles
    /// </summary>
    public sealed class UserMasterActor : ReceiveActor
    {
        ILoggingAdapter _log = Context.GetLogger();
        
        public UserMasterActor()
        {
            Receive<IUserProfileCommand>(c =>
            {
                var userActor = Context.Child($"{c.UserProfileId.ToString()}-userProfile").GetOrElse(() => StartChild(c.UserProfileId));
                userActor.Forward(c);
            });
        }

        protected override void PreStart()
        {
            _log.Info($"Starting user profile master actor...");
            base.PreStart();
        }

        protected override void PostStop()
        {
            _log.Info($"User profile master actor has been stopped");
            base.PostStop();
        }

        private IActorRef StartChild(Guid userProfileId)
        {
            string persistenceId = $"{userProfileId.ToString()}-userProfile";
            IActorRef mediator = DistributedPubSub.Get(Context.System).Mediator;
            return Context.ActorOf(Props.Create(() => new UserActor(userProfileId.ToString())), persistenceId);
        }
    }
}
