﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;
using AkkaToDoApp.Users.Domain;
using AkkaToDoApp.Users.Dto;
using System.Collections.Immutable;
using static AkkaToDoApp.Users.QueryModels;

namespace AkkaToDoApp.Users.Actors
{
    public class UserViewMasterActor : ReceiveActor, IWithUnboundedStash
    {
        public IStash Stash { get; set; }
        private readonly ILoggingAdapter _log = Context.GetLogger();

        private ICurrentAllEventsQuery _readJournal;

        public class BeginTrackUserProfiles
        {
            public IActorRef ShardRegion { get; }

            public BeginTrackUserProfiles(IActorRef shardRegion)
            {
                ShardRegion = shardRegion;
            }
        }

        public UserViewMasterActor(ICurrentAllEventsQuery readJournal)
        {
            _readJournal = readJournal;
            Running();
        }

        protected override void PreStart()
        {
            _log.Info("Starting user view master actor...");

            
        }

        protected override void PostStop()
        {
            _log.Info("User view master actor has been stopped");
        }

        private void WaitingForSharding()
        {
            Receive<BeginTrackUserProfiles>(b =>
            {
                var mediator = DistributedPubSub.Get(Context.System).Mediator;
                _log.Info("Recieved access to user profiles mediator. Starting user profiles views...");

                Become(Running);
                Stash.UnstashAll();
            });

            ReceiveAny(_ => Stash.Stash());
        }

        private void Running()
        {
            Receive<GetUserProfileByIdQuery>(q =>
            {
                var mat = Context.Materializer();
                var events = _readJournal
                    .CurrentAllEvents(NoOffset.Instance)
                    .Where(e => e.PersistenceId.Equals(q.UserProfileId + EntityIdHepler.UserProfileSuffix))
                    .Select(c => c.Event)
                    .RunAggregate(
                        ImmutableHashSet<object>.Empty,
                        (acc, c) => acc.Add(c),
                        mat)
                    .Result;

                var userProfileHistory = new UserProfileHistory(q.UserProfileId, events);
                Sender.Tell(userProfileHistory.UserProfileCurrentState);
            });

            Receive<GetUserProfileHistoryByIdQuery>(q =>
            {
                var mat = Context.Materializer();
                var events = _readJournal
                    .CurrentAllEvents(NoOffset.Instance)
                    .Where(e => e.PersistenceId.Equals(q.UserProfileId + EntityIdHepler.UserProfileSuffix))
                    .Select(c => c.Event)
                    .RunAggregate(
                        ImmutableHashSet<object>.Empty,
                        (acc, c) => acc.Add(c),
                        mat)
                    .Result;

                var userProfileHistory = new UserProfileHistory(q.UserProfileId, events);
                Sender.Tell(userProfileHistory);
            });

            Receive<GetUserProfilesHistoryQuery>(q =>
            {
                var mat = Context.Materializer();
                var events = _readJournal
                    .CurrentAllEvents(NoOffset.Instance)
                    .Select(c => c.Event)
                    .RunAggregate(
                        ImmutableHashSet<object>.Empty,
                        (acc, c) => acc.Add(c),
                        mat)
                    .Result;

                var res = events.OfType<IUserProfileEvent>()
                    .GroupBy(e => e.UserProfileId)
                    .Select(e => new UserProfileHistory(e.Key, e.ToImmutableHashSet<object>()))
                    .ToList();
                Sender.Tell(res);
            });
        }
    }
}
