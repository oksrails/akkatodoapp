﻿using Akka.Actor;
using Akka.Event;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;

namespace AkkaToDoApp.Users.Actors
{
    public class UserInitiatorActor : ReceiveActor
    {
        private readonly ILoggingAdapter _log = Context.GetLogger();
        private readonly IPersistenceIdsQuery _userIdsQuery;
        private readonly IActorRef _userQueryProxy;
        private readonly HashSet<string> _tickers = new HashSet<string>();

        private ICancelable? _heartbeatInterval;

        private class Heartbeat
        {
            public static readonly Heartbeat Instance = new Heartbeat();
            private Heartbeat() { }
        }

        public UserInitiatorActor(IPersistenceIdsQuery userIdsQuery, IActorRef userQueryProxy)
        {
            _userIdsQuery = userIdsQuery;
            _userQueryProxy = userQueryProxy;

            Receive<Ping>(p =>
            {
                _tickers.Add(p.UserProfileId);
                _userQueryProxy.Tell(p);
            });

            Receive<Heartbeat>(h =>
            {
                foreach (var ticker in _tickers)
                {
                    _userQueryProxy.Tell(new Ping(ticker));
                }
            });

            Receive<UnexpectedEndOfSream>(end =>
            {
                _log.Warning("Recieved unexpected end of PersistentIds stream. Restarting...");
                throw new ApplicationException("Restart me!");
            });
        }

        protected override void PreStart()
        {
            var mat = Context.Materializer();
            var self = Self;
            _userIdsQuery.PersistenceIds()
                .Where(x => x.EndsWith(EntityIdHepler.UserProfileSuffix))
                .Select(x => new Ping(EntityIdHepler.ExtractEntityIdFormPersistenceId(x)))
                .RunWith(Sink.ActorRef<Ping>(self, UnexpectedEndOfSream.Instance), mat);

            _heartbeatInterval = Context.System.Scheduler.ScheduleTellRepeatedlyCancelable(TimeSpan.FromSeconds(30),
                TimeSpan.FromSeconds(30), Self, Heartbeat.Instance, ActorRefs.NoSender);
        }

        protected override void PostStop()
        {
            _heartbeatInterval?.Cancel();
        }
    }
}
