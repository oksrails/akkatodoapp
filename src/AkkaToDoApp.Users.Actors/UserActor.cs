﻿using Akka.Actor;
using Akka.Event;
using Akka.Persistence;
using AkkaToDoApp.Users.Domain;
using AkkaToDoApp.Users.Dto;
using AkkaToDoApp.Common.Subscriptions;
using Akka.Cluster.Tools.PublishSubscribe;
using AkkaToDoApp.Common.Actors;

namespace AkkaToDoApp.Users.Actors
{
    public class UserActor : ReceivePersistentActor
    {
        ILoggingAdapter _log = Context.GetLogger();
        private const int SnapshotInterval = 10;
        public override string PersistenceId { get; }
        public Guid UserProfileId { get; }
        private UserProfile UserProfile { get; set; }
        private readonly IActorRef _mediator;
        private readonly string _userProfileProjectionTopic;
        private UserProfileProjection? UserProfileProjection { get; set; }
        public UserActor(string userProfileId)
        {
            UserProfileId = Guid.Parse(userProfileId);
            PersistenceId = userProfileId + EntityIdHepler.UserProfileSuffix;
            UserProfile = new UserProfile();
            _mediator = DistributedPubSub.Get(Context.System).Mediator;
            _userProfileProjectionTopic = TopicHelper.UserProfileProjectionTopic();

            Recovers();
            Commands();
        }

        protected override void PreStart()
        {
            _log.Info($"User actor {PersistenceId} has been started");
            base.PreStart();
        }

        protected override void PostStop()
        {
            _log.Info($"User actor {PersistenceId} has been stopped");
            base.PostStop();
        }

        private void Recovers()
        {
            Recover<SnapshotOffer>(offer =>
            {
                if (offer.Snapshot is UserProfileSnapshot userProfileSnapshot)
                {
                    UserProfile = new UserProfile(
                        userProfileSnapshot.UserProfileId,
                        userProfileSnapshot.FirstName,
                        userProfileSnapshot.LastName,
                        userProfileSnapshot.DisplayName);
                }
            });

            Recover<IUserProfileEvent>(c =>
            {
                UserProfile.Apply(c);
            });
        }

        private void Commands()
        {
            Command<Commands.V1.CreateUserProfile>(c =>
            {
                var userProfileCreated = new Events.V1.UserCreated(
                    c.UserProfileId,
                    c.FirstName,
                    c.LastName,
                    c.DisplayName);
                ProcessEvent(userProfileCreated);
            });

            Command<Commands.V1.ChangeFirstName>(c =>
            {
                var userFirstNameChanged = new Events.V1.UserFirstNameChanged(c.UserProfileId, c.FirstName);
                ProcessEvent(userFirstNameChanged);
            });

            Command<Commands.V1.ChangeLastName>(c =>
            {
                var userLastNameChanged = new Events.V1.UserLastNameChanged(c.UserProfileId, c.LastName);
                ProcessEvent(userLastNameChanged);
            });

            Command<Commands.V1.ChangeDisplayName>(c =>
            {
                var userDisplayNameChanged = new Events.V1.UserDisplayNameChanged(c.UserProfileId, c.DisplayName);
                ProcessEvent(userDisplayNameChanged);
            });

            Command<Commands.PublishProjection>(c =>
            {
                _mediator.Tell(new Publish(_userProfileProjectionTopic, UserProfileProjection));
            });
        }

        protected override void OnReplaySuccess()
        {
            GenerateUserProfileProjection();
        }

        private void ProcessEvent(IUserProfileEvent @event)
        {
            Persist(@event, evt =>
            {
                UserProfile.Apply(@event);
                if (LastSequenceNr % SnapshotInterval == 0)
                {
                    var userProfileSnapshot = new UserProfileSnapshot(
                        UserProfile.Id,
                        UserProfile.FirstName ?? "",
                        UserProfile.LastName ?? "",
                        UserProfile.DisplayName ?? "");
                    SaveSnapshot(userProfileSnapshot);
                }
                GenerateUserProfileProjection();
                _mediator.Tell(new Publish(_userProfileProjectionTopic, UserProfileProjection));
                _log.Info($"{@event.ToString()} for {PersistenceId} has been saved");
            });
        }

        private void GenerateUserProfileProjection()
        {
            UserProfileProjection = new UserProfileProjection(UserProfile.Id, UserProfile.FirstName ?? "", UserProfile.LastName ?? "", UserProfile.DisplayName ?? "");
        }
    }
}