﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using AkkaToDoApp.Users.Domain;
using AkkaToDoApp.Users.Dto;
using System.Collections.Immutable;
using static AkkaToDoApp.Users.Commands;
using static AkkaToDoApp.Users.QueryModels;

namespace AkkaToDoApp.Users.Actors
{
    public class UserViewActor : ReceiveActor, IWithUnboundedStash
    {
        private readonly ILoggingAdapter _log = Context.GetLogger();
        private IActorRef _userActorGateway;
        private readonly IActorRef _mediator;
        private IActorRef? _tickerEntity;
        private ICancelable? _pruneTimer;
        public IStash Stash { get; set; }
        private UserProfileHistory _userProfileHistory;
        private readonly string _tickerSymbol;
        private string _topic = "-userProfile";

        private sealed class Prune
        {
            public static readonly Prune Instance = new Prune();
            private Prune() { }
        }

        protected override void PreStart()
        {
            _log.Info("Starting user view actor...");
            Context.SetReceiveTimeout(TimeSpan.FromSeconds(5));
            _userActorGateway.Tell(new V1.FetchVolume(Guid.Parse(_tickerSymbol)));
            _pruneTimer = Context.System.Scheduler.ScheduleTellRepeatedlyCancelable(TimeSpan.FromMinutes(5),
                TimeSpan.FromMinutes(5), Self, Prune.Instance, ActorRefs.NoSender);
        }

        protected override void PostStop()
        {
            _pruneTimer?.Cancel();
            _log.Info("User view actor has been stopped");
        }

        public UserViewActor(string tickerSymbol, IActorRef mediator, IActorRef userActorGateway)
        {
            _tickerSymbol = tickerSymbol;
            _mediator = mediator;
            _userActorGateway = userActorGateway;
            _userProfileHistory = new UserProfileHistory(Guid.Parse(tickerSymbol), ImmutableHashSet<object>.Empty);

            WaitingForVolumes();
        }

        private void WaitingForVolumes()
        {
            Receive<UserProfileSnapshot>(s =>
            {
                _tickerEntity = Sender;
                _mediator.Tell(new Subscribe(_topic, Self));
            });

            Receive<SubscribeAck>(ack =>
            {
                _log.Info($"Subscribed to {_topic} - ready for real-time processing");
                Become(Processing);
                Context.Watch(_tickerEntity);
                Context.SetReceiveTimeout(null);
            });

            Receive<ReceiveTimeout>(ack =>
            {
                _log.Warning($"Received no initial values for [{_tickerSymbol}] from source of truth. Retrying...");
                _userActorGateway.Tell(new V1.FetchVolume(Guid.Parse(_tickerSymbol)));
            });
        }

        private void Processing()
        {
            Receive<IUserProfileEvent>(e =>
            {
                _userProfileHistory = _userProfileHistory.AppendUserProfileEvent(e);
            });

            Receive<GetUserProfileHistoryByIdQuery>(q =>
            {
                Sender.Tell(_userProfileHistory.UserProfileCurrentState); 
            });

            Receive<Terminated>(t =>
            {
                if (t.ActorRef.Equals(_tickerEntity))
                {
                    _log.Warning("Source of truth entity terminated. Re-acquiring...");
                    Context.SetReceiveTimeout(TimeSpan.FromSeconds(5));
                    _userActorGateway.Tell(new V1.FetchVolume(Guid.Parse(_tickerSymbol)));
                    _mediator.Tell(new Unsubscribe(_topic, Self));
                    Become(WaitingForVolumes);
                }
            });
        }
    }
}