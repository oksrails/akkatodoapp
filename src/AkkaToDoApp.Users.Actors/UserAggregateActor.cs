﻿using Akka.Event;
using Akka.Persistence;
using Akka.Persistence.Query;
using static AkkaToDoApp.Users.Commands;

namespace AkkaToDoApp.Users.Actors
{
    /// <summary>
    /// Actor for aggregation user profile events
    /// </summary>
    public class UserAggregateActor : ReceivePersistentActor
    {
        public override string PersistenceId { get; }
        private readonly ILoggingAdapter _log = Context.GetLogger();
        public const int SnapshotEveryN = 10;
        public long QueryOffset { get; private set; }

        private record PublishEvents
        {
            public static readonly PublishEvents Instance = new PublishEvents();

            private PublishEvents() { }
        }

        public UserAggregateActor(string persistenceId)
        {
            PersistenceId = persistenceId;
        }

        private void Receives()
        {

        }

        private void Commands()
        {
            Command<EventEnvelope>(e =>
            {

            });

            Command<V1.FetchVolume>(f =>
            {

            });

            Command<PublishEvents>(p =>
            {

            });

            Command<Ping>(p =>
            {
                if (_log.IsDebugEnabled)
                {
                    _log.Debug($"Pinged via {Sender}");
                }
            });

            Command<SaveSnapshotSuccess>(s =>
            {
                DeleteSnapshots(new SnapshotSelectionCriteria(s.Metadata.SequenceNr-1));
                DeleteMessages(s.Metadata.SequenceNr);
            });
        }

        private void RecoverAggregateData()
        {

        }

        protected override void OnReplaySuccess()
        {
            base.OnReplaySuccess();
        }


    }
}
