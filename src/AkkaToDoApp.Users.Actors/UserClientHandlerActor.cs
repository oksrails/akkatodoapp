﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Persistence.Query;
using Akka.Streams;
using Akka.Streams.Dsl;
using AkkaToDoApp.Common.Actors;
using AkkaToDoApp.Common.Subscriptions;
using AkkaToDoApp.Users.Subscriptions;
using System.Collections.Immutable;
using static AkkaToDoApp.Users.Commands;

namespace AkkaToDoApp.Users.Actors
{
    public class UserClientHandlerActor : ReceiveActor
    {
        private ILoggingAdapter _log = Context.GetLogger();
        private IActorRef _mediator;
        private IActorRef _shardRegion;
        private ICurrentPersistenceIdsQuery _currentPersistenceIds;

        public UserClientHandlerActor(IActorRef shardRegion, ICurrentPersistenceIdsQuery currentPersistenceIds)
        {
            _shardRegion = shardRegion;
            _mediator = DistributedPubSub.Get(Context.System).Mediator;
            _currentPersistenceIds = currentPersistenceIds;

            ReceiveAsync<SubscribeUserProfileClient>(async s =>
            {
                _log.Info($"Received {s} from {Sender}");
                await _mediator.Ask(new Subscribe(TopicHelper.UserProfileProjectionTopic(), s.Subscriber));
                Context.Watch(s.Subscriber);
                SendPublishMessage();
            });

            Receive<IUserProfileCommand>(c =>
            {
                _shardRegion.Forward(c);
            });

            ReceiveAsync<Terminated>(async t =>
            {
                await _mediator.Ask(new Unsubscribe(TopicHelper.UserProfileProjectionTopic(), Sender));
            });
        }

        protected override void PreStart()
        {
            _log.Info($"Starting user profile cluster client...");
        }

        protected override void PostStop()
        {
            _log.Info($"User profile cluster client stopped");
        }

        private void SendPublishMessage()
        {
            ActorMaterializer mat = Context.Materializer();
            var userProfileIds = _currentPersistenceIds
                .CurrentPersistenceIds()
                .Where(u => u.EndsWith(EntityIdHepler.UserProfileSuffix))
                .RunAggregate(
                    ImmutableHashSet<Guid>.Empty,
                    (acc, c) => acc.Add(Guid.Parse(EntityIdHepler.ExtractEntityIdFormPersistenceId(c))),
                    mat)
                .Result;

            foreach (var userProfileId in userProfileIds)
            {
                _shardRegion.Tell(new PublishProjection(userProfileId));
            }
        }
    }
}
