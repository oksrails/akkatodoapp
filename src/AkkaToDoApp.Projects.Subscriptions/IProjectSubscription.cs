﻿namespace AkkaToDoApp.Projects.Subscriptions
{
    public interface IProjectSubscription
    {
        Guid ProjectId { get; init; }
    }
}
