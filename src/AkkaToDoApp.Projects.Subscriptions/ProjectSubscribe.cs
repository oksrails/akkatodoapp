﻿using Akka.Actor;

namespace AkkaToDoApp.Projects.Subscriptions
{
    public record ProjectSubscribe : IProjectSubscription
    {
        public Guid ProjectId { get; init; }
        public IActorRef Subscriber;

        public ProjectSubscribe(IActorRef subscriber)
        {
            Subscriber = subscriber;
        }
    }
}
