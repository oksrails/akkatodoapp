﻿using Akka.Actor;

namespace AkkaToDoApp.Projects.Subscriptions
{
    public record SubscribeProjectClient
    {
        public IActorRef Subscriber { get; init; }

        public SubscribeProjectClient(IActorRef subscriber) 
        {
            Subscriber = subscriber;
        }
    }
}