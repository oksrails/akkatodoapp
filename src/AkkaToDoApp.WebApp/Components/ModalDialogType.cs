﻿namespace AkkaToDoApp.WebApp.Components
{
    public enum ModalDialogType
    {
        Create,
        Edit
    }
}
