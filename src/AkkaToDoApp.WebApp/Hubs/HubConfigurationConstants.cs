﻿namespace AkkaToDoApp.WebApp.Hubs
{
    public sealed class HubConfigurationConstants
    {
        public const string ProjectHubName = "/hubs/project";
        public const string UserHubName = "/hubs/user";
        public const string ProjectTaskHubName = "/hubs/projectTask";
    }
}
