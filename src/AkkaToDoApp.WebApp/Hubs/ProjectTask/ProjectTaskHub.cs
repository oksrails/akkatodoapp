﻿using AkkaToDoApp.WebApp.Services;
using Microsoft.AspNetCore.SignalR;
using static AkkaToDoApp.ProjectTasks.Commands;
using static AkkaToDoApp.ProjectTasks.QueryModels;

namespace AkkaToDoApp.WebApp.Hubs.ProjectTask
{
    public class ProjectTaskHub : Hub
    {
        private readonly IMessageHandler _handler;

        public ProjectTaskHub(IMessageHandler handler)
        {
            _handler = handler;
        }

        public void FetchProjectTaskProjections()
        {
            FetchProjectTaskProjections query = ProjectTasks.QueryModels.FetchProjectTaskProjections.Instance;
        }

        public void CreateTask(V1.CreateProjectTask command)
        {
            _handler.Handle(command);
        }

        public void ChangeProjectTaskTitle(V1.ChangeProjectTaskTitle command)
        {
            _handler.Handle(command);
        }

        public void ChangeProjectTaskDescription(V1.ChangeProjectTaskDescription command)
        {
            _handler.Handle(command);
        }

        public void ChangeProjectTaskType(V1.ChangeProjectTaskTitle command)
        {
            _handler.Handle(command);
        }

        public void ChangeProjectTaskEstimation(V1.ChangeProjectTaskEstimation command)
        {
            _handler.Handle(command);
        }

        public void AssignProjectTask(V1.AssignProjectTask command)
        {
            _handler.Handle(command);
        }

        public void StartProjectTaskWork(V1.StartProjectTasKWork command)
        {
            _handler.Handle(command);
        }

        public void CompliteProjectTask(V1.CompleteProjectTask command)
        {
            _handler.Handle(command);
        }
    }
}
