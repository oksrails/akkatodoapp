﻿using AkkaToDoApp.ProjectTasks.Dto;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;

namespace AkkaToDoApp.WebApp.Hubs.ProjectTask
{
    public class ProjectTaskHubHelper
    {
        private readonly IHubContext<ProjectTaskHub> _hub;

        public ProjectTaskHubHelper(IHubContext<ProjectTaskHub> hub)
        {
            _hub = hub;
        }

        public async Task WriteProjections(List<ProjectTaskProjection> projections)
        {
            await WriteMessage(projections);
        }

        private async Task WriteMessage(List<ProjectTaskProjection> projections)
        {
            var cts = new CancellationTokenSource(TimeSpan.FromMilliseconds(500));
            await _hub.Clients.All.SendAsync("FetchProjectTaskProjections", projections, cts);
        }
    }
}
