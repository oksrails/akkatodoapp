﻿using Microsoft.AspNetCore.SignalR;
using AkkaToDoApp.WebApp.Services;
using static AkkaToDoApp.Users.Commands;
using static AkkaToDoApp.Users.QueryModels;

namespace AkkaToDoApp.WebApp.Hubs.UserProfile
{
    public class UserProfileHub : Hub
    {
        private readonly IMessageHandler _handler;

        public UserProfileHub(IMessageHandler handler)
        {
            _handler = handler;
        }

        public void UpdateUserProfileList()
        {
            var query = FetchUserProfileProjections.Instance;
            _handler.Handle(query);
        }

        public void CreateUserProfile(V1.CreateUserProfile command)
        {
            _handler.Handle(command);
        }

        public void ChangeFirstName(V1.ChangeFirstName command)
        {
            _handler.Handle(command);
        }

        public void ChangeLastName(V1.ChangeLastName command)
        {
            _handler.Handle(command);
        }

        public void ChangeDisplayName(V1.ChangeDisplayName command)
        {
            _handler.Handle(command);
        }
    }
}
