﻿using AkkaToDoApp.Users.Dto;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;

namespace AkkaToDoApp.WebApp.Hubs.UserProfile
{
    public class UserProfileHubHelper
    {
        private readonly IHubContext<UserProfileHub> _hub;

        public UserProfileHubHelper(IHubContext<UserProfileHub> hub)
        {
            _hub = hub;
        }

        public async Task WriteProjections(List<UserProfileProjection> projections)
        {
            await WriteMessage(projections);
        }

        private async Task WriteMessage(List<UserProfileProjection> projections)
        {
            var cts = new CancellationTokenSource(TimeSpan.FromMilliseconds(500));
            await _hub.Clients.All.SendAsync("FetchUserProfileProjections", projections, cts.Token);
        }
    }
}
