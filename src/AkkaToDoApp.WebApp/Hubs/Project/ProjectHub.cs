﻿using AkkaToDoApp.WebApp.Services;
using Microsoft.AspNetCore.SignalR;
using static AkkaToDoApp.Projects.Commands;
using static AkkaToDoApp.Projects.QueryModels;

namespace AkkaToDoApp.WebApp.Hubs.Project
{
    public class ProjectHub : Hub
    {
        private readonly IMessageHandler _handler;

        public ProjectHub(IMessageHandler handler)
        {
            _handler = handler;
        }

        public void UpdateProjectList()
        {
            FetchProjectProjections query = FetchProjectProjections.Instance;
            _handler.Handle(query);
        }

        public void CreateProject(V1.CreateProject command)
        {
            _handler.Handle(command);
        }

        public void ChangeProjectTitle(V1.ChangeProjectTitle command)
        {
            _handler.Handle(command);
        }

        public void ChangeProjectDescription(V1.ChangeProjectDescription command)
        {
            _handler.Handle(command);
        }
    }
}
