﻿using AkkaToDoApp.Projects.Dto;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;

namespace AkkaToDoApp.WebApp.Hubs.Project
{
    public class ProjectHubHelper
    {
        private readonly IHubContext<ProjectHub> _hub;

        public ProjectHubHelper(IHubContext<ProjectHub> hub)
        {
            _hub = hub;
        }

        public async Task WriteProjection(List<ProjectProjection> projections)
        {
            await WriteMessage(projections);
        }

        private async Task WriteMessage(List<ProjectProjection> projections)
        {
            var cts = new CancellationTokenSource(TimeSpan.FromMilliseconds(500));
            await _hub.Clients.All.SendAsync("writeProjectProjection", projections, cts.Token);
        }
    }
}
