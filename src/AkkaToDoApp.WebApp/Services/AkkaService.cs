﻿using Akka.Actor;
using Akka.Cluster.Tools.Client;
using Akka.Configuration;
using Akka.DependencyInjection;
using AkkaToDoApp.Projects;
using AkkaToDoApp.ProjectTasks;
using AkkaToDoApp.Users;
using AkkaToDoApp.WebApp.Actors.Project;
using AkkaToDoApp.WebApp.Actors.UserProfiles;
using AkkaToDoApp.WebApp.Actors.ProjectTask;
using Akka.Bootstrap.Docker;

namespace AkkaToDoApp.WebApp.Services
{
    public class AkkaService : IHostedService, IMessageHandler
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IHostApplicationLifetime _applicationLifetime;
        private IActorRef UserProfilePublisherActor;
        private IActorRef ProjectPublisherActor;
        private IActorRef ProjectTaskPublisherActor;
        private IActorRef MasterClient;
        private ActorSystem _actorSystem;

        public AkkaService(IServiceProvider serviceProvider, IHostApplicationLifetime applicationLifetime)
        {
            _serviceProvider = serviceProvider;
            _applicationLifetime = applicationLifetime;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var conf = ConfigurationFactory.ParseString(File.ReadAllText("app.conf")).BootstrapFromDocker();
            var bootstrap = BootstrapSetup.Create()
                .WithConfig(conf);
            var diSetup = DependencyResolverSetup.Create(_serviceProvider);
            var actorSystemSetup = bootstrap.And(diSetup);

            _actorSystem = ActorSystem.Create("AkkaToDoWebApp", actorSystemSetup);

            //var initialContactAdress = ClusterClientSettings.Create(_actorSystem).InitialContacts.Select(x => x.Address).ToList();
            var initialContactAdress = Environment.GetEnvironmentVariable("CLUSTER_SEEDS")?.Trim().Split(",").Select(x => Address.Parse(x)).ToList();
            var dr = DependencyResolver.For(_actorSystem);

            UserProfilePublisherActor = _actorSystem.ActorOf(dr.Props<UserProfilePublisherActor>(), "userProfilePublisher");
            ProjectPublisherActor = _actorSystem.ActorOf(dr.Props<ProjectPublisherActor>(), "projectPublisher");
            ProjectTaskPublisherActor = _actorSystem.ActorOf(dr.Props<ProjectTaskPublisherActor>(), "projectTaskPublisher");

            MasterClient = _actorSystem.ActorOf(Props.Create(() => new MasterClientActor(UserProfilePublisherActor, ProjectPublisherActor, ProjectTaskPublisherActor, initialContactAdress)), "configurator");

            _actorSystem.WhenTerminated.ContinueWith(x => _applicationLifetime.StopApplication());

            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _actorSystem.Terminate();
        }

        public void Handle(IProjectCommand command)
        {
            MasterClient.Tell(command);
        }

        public void Handle(IProjectQuery query)
        {
            ProjectPublisherActor.Tell(query);
        }

        public void Handle(IUserProfileCommand command)
        {
            MasterClient.Tell(command);
        }

        public void Handle(IUserQuery query)
        {
            UserProfilePublisherActor.Tell(query);
        }

        public void Handle(IProjectTaskCommand command)
        {
            MasterClient.Tell(command);
        }

        public void Handle(IProjectTaskQuery query)
        {
            ProjectTaskPublisherActor.Tell(query);
        }
    }
}
