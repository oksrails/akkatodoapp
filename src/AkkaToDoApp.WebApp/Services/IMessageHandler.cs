﻿using AkkaToDoApp.Projects;
using AkkaToDoApp.Users;
using AkkaToDoApp.ProjectTasks;

namespace AkkaToDoApp.WebApp.Services
{
    public interface IMessageHandler
    {
        public void Handle(IProjectCommand command);
        public void Handle(IProjectQuery query);
        public void Handle(IUserProfileCommand command);
        public void Handle(IUserQuery query);
        public void Handle(IProjectTaskCommand command);
        public void Handle(IProjectTaskQuery query);
    }
}
