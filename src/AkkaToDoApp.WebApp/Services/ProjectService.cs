﻿using AkkaToDoApp.Projects.Dto;

namespace AkkaToDoApp.WebApp.Services
{
    public class ProjectService
    {
        private List<ProjectProjection> Projections = new List<ProjectProjection>();

        public void AppendProejction(ProjectProjection projectProjection)
        {
            Projections.Add(projectProjection);
        }

        public List<ProjectProjection> GetProjections()
        {
            return Projections;
        }
    }
}
