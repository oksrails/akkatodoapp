﻿using Akka.Actor;
using Akka.Event;
using AkkaToDoApp.Users.Dto;
using AkkaToDoApp.WebApp.Hubs.UserProfile;
using static AkkaToDoApp.Users.QueryModels;

namespace AkkaToDoApp.WebApp.Actors.UserProfiles
{
    public class UserProfilePublisherActor : ReceiveActor
    {
        private readonly ILoggingAdapter _log = Context.GetLogger();
        private readonly UserProfileHubHelper _hub;
        private List<UserProfileProjection> Projections = new List<UserProfileProjection>();

        public UserProfilePublisherActor(UserProfileHubHelper hub)
        {
            _hub = hub;
            Processing();
        }

        private void Processing()
        {
            Receive<UserProfileProjection>(p =>
            {
                try
                {
                    _log.Debug($"Received projection for user profile id: {p.UserProfileId}");
                    ProcessProjection(p);
                    _hub.WriteProjections(Projections).Wait();
                }
                catch (Exception ex)
                {
                    _log.Error(ex, $"Error while writing projection for user profile id: {p.UserProfileId}");
                }
            });

            Receive<FetchUserProfileProjections>(f =>
            {
                _hub.WriteProjections(Projections).Wait();
            });
        }

        private void ProcessProjection(UserProfileProjection projection)
        {
            if (Projections.Any())
            {
                UserProfileProjection? oldProjection = Projections.SingleOrDefault(p => p.UserProfileId.Equals(projection.UserProfileId));
                if (oldProjection is not null)
                {
                    Projections.Remove(oldProjection);
                }
            }
            Projections.Add(projection);
            Projections = Projections.OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ToList();
        }
    }
}
