﻿using Akka.Actor;
using Akka.Event;
using AkkaToDoApp.ProjectTasks.Dto;
using AkkaToDoApp.WebApp.Hubs.ProjectTask;
using static AkkaToDoApp.ProjectTasks.QueryModels;

namespace AkkaToDoApp.WebApp.Actors.ProjectTask
{
    public class ProjectTaskPublisherActor : ReceiveActor
    {
        private readonly ILoggingAdapter _log = Context.GetLogger();
        private readonly ProjectTaskHubHelper _hub;
        private List<ProjectTaskProjection> Projections = new List<ProjectTaskProjection>();

        public ProjectTaskPublisherActor(ProjectTaskHubHelper hub)
        {
            _hub = hub;
            Processing();
        }

        private void Processing()
        {
            Receive<ProjectTaskProjection>(p =>
            {
                try
                {
                    _log.Debug($"Received projection for project task id: {p.ProjectTaskId}");
                    ProcessProjection(p);
                    _hub.WriteProjections(Projections).Wait();
                }
                catch (Exception ex)
                {
                    _log.Error(ex, $"Error while writing projection for project task id: {p.ProjectTaskId}");
                }
            });

            Receive<FetchProjectTaskProjections>(f =>
            {
                _hub.WriteProjections(Projections).Wait();
            });
        }

        private void ProcessProjection(ProjectTaskProjection projection)
        {
            if (Projections.Any())
            {
                ProjectTaskProjection? oldProjection = Projections.SingleOrDefault(p => p.ProjectTaskId.Equals(projection.ProjectTaskId));
                if (oldProjection is not null)
                {
                    Projections.Remove(oldProjection);
                }
            }
            Projections.Add(projection);
            Projections = Projections.OrderBy(p => p.ProjectTitle).ToList();
        }
    }
}