﻿namespace AkkaToDoApp.WebApp.Actors
{
    public sealed class ActorConfigurationConstants
    {
        public const string ClusterReceptionist = "system/receptionist";
        public const string UserProfileClientHandler = "/user/userProfileClientHandler";
        public const string UserProfiles = "/user/userProfiles";
        public const string ProjectSubscription = "/user/projectSubscriptions";
        public const string Projects = "/user/projects";
        public const string ProjectTaskSuscription = "/user/projectTaskSubscriptions";
        public const string ProjectTasks = "/user/projectTasks";
    }
}
