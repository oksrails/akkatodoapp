﻿namespace AkkaToDoApp.WebApp.Actors.Commands
{
    public record Start
    {
        public static readonly Start Instance = new Start();

        private Start() { }
    }
}
