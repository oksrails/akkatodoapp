﻿using Akka.Actor;
using Akka.Event;
using AkkaToDoApp.Projects.Dto;
using AkkaToDoApp.WebApp.Hubs.Project;
using static AkkaToDoApp.Projects.QueryModels;

namespace AkkaToDoApp.WebApp.Actors.Project
{
    public class ProjectPublisherActor : ReceiveActor
    {
        private readonly ILoggingAdapter _log = Context.GetLogger();
        private readonly ProjectHubHelper _hub;
        private List<ProjectProjection> Projections = new List<ProjectProjection>();

        public ProjectPublisherActor(ProjectHubHelper hub)
        {
            _hub = hub;
            Processing();
        }

        private void Processing()
        {
            Receive<ProjectProjection>(p =>
            {
                try
                {
                    _log.Debug($"Received projection for project id: {p.ProjectId}");
                    ProcessProjection(p);
                    _hub.WriteProjection(Projections).Wait();
                }
                catch (Exception ex)
                {
                    _log.Error(ex, $"Error while writing projection for project id: {p.ProjectId}");
                }
            });

            Receive<FetchProjectProjections>(f =>
            {
                _hub.WriteProjection(Projections).Wait();
            });
        }

        private void ProcessProjection(ProjectProjection projection)
        {
            if (Projections.Any())
            {
                ProjectProjection? oldProjection = Projections.SingleOrDefault(p => p.ProjectId.Equals(projection.ProjectId));
                if (oldProjection is not null)
                {
                    Projections.Remove(oldProjection);
                }
            }
            Projections.Add(projection);
            Projections = Projections.OrderBy(p => p.ProjectTitle).ToList();
        }
    }
}
