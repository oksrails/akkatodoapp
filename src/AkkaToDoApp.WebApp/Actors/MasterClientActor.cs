﻿using Akka.Actor;
using Akka.Cluster.Tools.Client;
using Akka.Event;
using AkkaToDoApp.Projects;
using AkkaToDoApp.Projects.Subscriptions;
using AkkaToDoApp.Users;
using AkkaToDoApp.Users.Subscriptions;
using AkkaToDoApp.ProjectTasks;
using AkkaToDoApp.ProjectTasks.Subscriptions;
using AkkaToDoApp.WebApp.Actors.Commands;
using System.Collections.Immutable;

namespace AkkaToDoApp.WebApp.Actors.Project
{
    public class MasterClientActor : ReceiveActor
    {
        private readonly ILoggingAdapter _log = Context.GetLogger();
        private IActorRef? _clusterClient;
        private readonly IActorRef _userProfilePublisher;
        private readonly IActorRef _projectPublisher;
        private readonly IActorRef _projectTaskPublisher;
        private ImmutableHashSet<ActorPath> _initialContacts;

        public MasterClientActor(
            IActorRef userProfilePublisher,
            IActorRef projectPublisher,
            IActorRef projectTaskPublisher,
            IReadOnlyList<Address> contactAddresses)
        {
            _userProfilePublisher = userProfilePublisher;
            _projectPublisher = projectPublisher;
            _projectTaskPublisher = projectTaskPublisher;
            var cont = new RootActorPath(contactAddresses.First());
            _initialContacts = contactAddresses.Select(x => new RootActorPath(x) / "system" / "receptionist").ToImmutableHashSet();
            Initializing();
        }

        private void Initializing()
        {
            Receive<Start>(s =>
            {
                _log.Debug($"Sending subscribe requests to cluster...");
                _clusterClient.Tell(new ClusterClient.Send(ActorConfigurationConstants.UserProfileClientHandler, new SubscribeUserProfileClient(_userProfilePublisher)));
                _clusterClient.Tell(new ClusterClient.Send(ActorConfigurationConstants.ProjectSubscription, new SubscribeProjectClient(_projectPublisher)));
                //_clusterClient.Tell(new ClusterClient.Send(ActorConfigurationConstants.ProjectTaskSuscription, new SubscribeProjectTaskClient(_projectTaskPublisher)));
            });

            Receive<ReceiveTimeout>(timeout => Self.Tell(Start.Instance));

            Receive<IProjectCommand>(c =>
            {
                _log.Debug($"Received project command {nameof(c)}");
                _clusterClient.Tell(new ClusterClient.Send(ActorConfigurationConstants.Projects, c));
            });

            Receive<IUserProfileCommand>(c =>
            {
                _log.Debug($"Received user profile command {nameof(c)}");
                _clusterClient.Tell(new ClusterClient.Send(ActorConfigurationConstants.UserProfileClientHandler, c));
            });

            Receive<IProjectTaskCommand>(c =>
            {
                _clusterClient.Tell(new ClusterClient.Send(ActorConfigurationConstants.ProjectTasks, c));
            });
        }

        protected override void PreStart()
        {
            Context.System.Scheduler.ScheduleTellOnce(TimeSpan.FromSeconds(2), Self, Start.Instance, ActorRefs.NoSender);
            _clusterClient = Context.ActorOf(ClusterClient.Props(ClusterClientSettings.Create(Context.System).WithInitialContacts(_initialContacts)));
        }
    }
}
