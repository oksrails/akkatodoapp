﻿using System.ComponentModel.DataAnnotations;

namespace AkkaToDoApp.WebApp.Dto
{
    public class UserProfileDto
    {
        public Guid UserProfileId { get; set; }
        
        [Required]
        [StringLength(20, ErrorMessage = "First name must not be longer then 20 characters.")]
        public string? FirstName { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Last name must not be longer then 20 characters.")]
        public string? LastName { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Display name must not be longer then 20 characters.")]
        public string? DisplayName { get; set; }
    }
}
