﻿using System.ComponentModel.DataAnnotations;

namespace AkkaToDoApp.WebApp.Dto
{
    public record ProjectDto
    {
        [Required]
        public Guid ProjectId { get; set; }
        [Required]
        public string? ProjectTitle { get; set; }
        [Required]
        public string? ProjectDescription { get; set; }
        [Required]
        public Guid ProjectOwnerId { get; set; }
    }
}
