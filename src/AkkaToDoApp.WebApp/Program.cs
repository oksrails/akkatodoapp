using AkkaToDoApp.WebApp.Hubs;
using AkkaToDoApp.WebApp.Hubs.Project;
using AkkaToDoApp.WebApp.Hubs.ProjectTask;
using AkkaToDoApp.WebApp.Hubs.UserProfile;
using AkkaToDoApp.WebApp.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddMvc();
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddSignalR();
builder.Services.AddSingleton<IMessageHandler, AkkaService>();
builder.Services.AddHostedService<AkkaService>(sp => (AkkaService)sp.GetRequiredService<IMessageHandler>());
builder.Services.AddTransient<UserProfileHubHelper>();
builder.Services.AddTransient<ProjectHubHelper>();
builder.Services.AddTransient<ProjectTaskHubHelper>();
builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    options.ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseForwardedHeaders();

app.UseStaticFiles();

app.UseRouting();

app.MapRazorPages();
app.MapBlazorHub();

app.MapHub<UserProfileHub>(HubConfigurationConstants.UserHubName);
app.MapHub<ProjectHub>(HubConfigurationConstants.ProjectHubName);
app.MapHub<ProjectTaskHub>(HubConfigurationConstants.ProjectTaskHubName);

app.MapFallbackToPage("/_Host");

app.Run();
