﻿namespace AkkaToDoApp.Users
{
    public interface IUserProfileCommand
    {
        Guid UserProfileId { get; init; }
    }
}
