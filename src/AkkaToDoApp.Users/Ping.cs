﻿namespace AkkaToDoApp.Users
{
    public sealed class Ping : IEquatable<Ping>
    {
        public string UserProfileId { get; }

        public Ping(string userProfileId)
        {
            UserProfileId = userProfileId;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is Ping other && Equals(other);
        }

        public bool Equals(Ping? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(UserProfileId, other.UserProfileId);
        }

        public override int GetHashCode()
        {
            return UserProfileId.GetHashCode();
        }

        public static bool operator ==(Ping left, Ping right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Ping left, Ping right)
        {
            return !Equals(left, right);
        }
    }
}
