﻿namespace AkkaToDoApp.Users.Dto
{
    public record UserProfileSnapshot
    {
        public Guid UserProfileId { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string DisplayName { get; }

        public UserProfileSnapshot(
            Guid userProfileId,
            string firstName,
            string lastName,
            string displayName)
        {
            UserProfileId = userProfileId;
            FirstName = firstName;
            LastName = lastName;
            DisplayName = displayName;
        }
    }
}
