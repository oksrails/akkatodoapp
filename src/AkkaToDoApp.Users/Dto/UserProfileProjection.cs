﻿namespace AkkaToDoApp.Users.Dto
{
    public class UserProfileProjection
    {
        public Guid UserProfileId { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string DisplayName { get; }

        public UserProfileProjection(
            Guid userProfileId,
            string firstName,
            string lastName,
            string displayName)
        {
            UserProfileId = userProfileId;
            FirstName = firstName;
            LastName = lastName;
            DisplayName = displayName;
        }
    }
}
