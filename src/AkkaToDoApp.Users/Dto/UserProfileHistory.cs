﻿using AkkaToDoApp.Users.Domain;
using System.Collections.Immutable;

namespace AkkaToDoApp.Users.Dto
{
    /// <summary>
    /// In-memory user profile replicated view
    /// </summary>
    public class UserProfileHistory
    {
        public Guid UserProfileId { get; init; }
        public UserProfile UserProfileCurrentState { get; }
        public ImmutableHashSet<object> UserEvents { get; }

        public UserProfileHistory(Guid userProfileId, ImmutableHashSet<object> userEvents)
        {
            UserProfileId = userProfileId;
            UserEvents = userEvents;
            UserProfileCurrentState = new UserProfile();
            if (UserEvents.Any())
            {
                UserProfileCurrentState.Load(userEvents);
            }
        }

        public UserProfileHistory AppendUserProfileEvent(IUserProfileEvent userEvent)
        {
            if (!userEvent.UserProfileId.Equals(UserProfileId))
                throw new ArgumentException($"Expected user profile id: {UserProfileId}, but received {userEvent.UserProfileId}", nameof(userEvent));

            return new UserProfileHistory(UserProfileId, UserEvents.Add(userEvent));
        }
    }
}
