﻿namespace AkkaToDoApp.Users
{
    public record Commands
    {
        public record PublishProjection : IUserProfileCommand
        {
            public Guid UserProfileId { get; init; }

            public PublishProjection(Guid userProfileId)
            {
                UserProfileId = userProfileId;
            }
        }

        public record V1
        {
            public record CreateUserProfile : IUserProfileCommand
            {
                public Guid UserProfileId { get; init; }
                public string FirstName { get; init; }
                public string LastName { get; init; }
                public string DisplayName { get; init; }

                public CreateUserProfile(
                    Guid userProfileId,
                    string firstName,
                    string lastName,
                    string displayName)
                {
                    UserProfileId = userProfileId;
                    FirstName = firstName;
                    LastName = lastName;
                    DisplayName = displayName;
                }

                public override string ToString() => $"{nameof(CreateUserProfile)}";
            }

            public record ChangeFirstName : IUserProfileCommand
            {
                public Guid UserProfileId { get; init; }
                public string FirstName { get; init; }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
                public ChangeFirstName() { }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

                public ChangeFirstName(
                    Guid userProfileId,
                    string firstName)
                {
                    UserProfileId = userProfileId;
                    FirstName = firstName;
                }

                public override string ToString() => $"{nameof(ChangeFirstName)}";
            }

            public record ChangeLastName : IUserProfileCommand
            {
                public Guid UserProfileId { get; init; }
                public string LastName { get; init; }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
                public ChangeLastName() { }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

                public ChangeLastName(
                    Guid userProfileId,
                    string lastName)
                {
                    UserProfileId = userProfileId;
                    LastName = lastName;
                }

                public override string ToString() => $"{nameof(ChangeLastName)}";
            }

            public record ChangeDisplayName : IUserProfileCommand
            {
                public Guid UserProfileId { get; init; }
                public string DisplayName { get; init; }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
                public ChangeDisplayName() { }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

                public ChangeDisplayName(
                    Guid userProfileId,
                    string displayName)
                {
                    UserProfileId = userProfileId;
                    DisplayName = displayName;
                }

                public override string ToString() => $"{nameof(ChangeDisplayName)}";
            }

            public record FetchVolume : IUserProfileCommand
            {
                public Guid UserProfileId { get; init; }

                public FetchVolume(Guid userProfileId)
                {
                    UserProfileId = userProfileId;
                }
            }
        }
    }
}