﻿namespace AkkaToDoApp.Users
{
    public record QueryModels
    {
        public record FetchUserProfileProjections : IUserQuery
        {
            public static FetchUserProfileProjections Instance = new FetchUserProfileProjections();
            private FetchUserProfileProjections() {}
        }

        public record GetUserProfileByIdQuery : IUserQuery
        {
            public Guid UserProfileId { get; set; }
        }

        public record GetUserProfileHistoryByIdQuery : IUserQuery
        {
            public Guid UserProfileId { get; set; }
        }

        public record GetUserProfilesHistoryQuery : IUserQuery { }
    }
}
