﻿namespace AkkaToDoApp.Common.Actors
{
    public static class EntityIdHepler
    {
        public const string UserProfileSuffix = "-userProfile";
        public const string ProjectSuffix = "-project";
        public const string ProjectTaskSuffix = "-projectTask";

        public static string ExtractEntityIdFormPersistenceId(string persistenceId)
        {
            int index = persistenceId.LastIndexOf("-");
            var result = persistenceId.Substring(0, index);
            return result;
        }
    }
}
