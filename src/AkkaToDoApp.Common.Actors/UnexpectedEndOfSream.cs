﻿namespace AkkaToDoApp.Common.Actors
{
    public class UnexpectedEndOfSream
    {
        public static readonly UnexpectedEndOfSream Instance = new UnexpectedEndOfSream();
        private UnexpectedEndOfSream() { }
    }
}