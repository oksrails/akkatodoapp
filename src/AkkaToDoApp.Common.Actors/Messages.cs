﻿namespace AkkaToDoApp.Common.Actors
{
    public record Messages
    {
        public record Request
        {
            public record CheckRunningRequest
            {
            }
        }

        public record Response
        {
            public record CheckRunningResponse
            {
                public bool IsRunning { get; init; }

                public CheckRunningResponse(bool isRunning)
                {
                    IsRunning = isRunning;
                }
            }
        }
    }
}
