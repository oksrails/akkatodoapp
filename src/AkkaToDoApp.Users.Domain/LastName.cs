﻿using static System.String;

namespace AkkaToDoApp.Users.Domain
{
    public record LastName
    {
        public string Value { get; }

        internal LastName(string value) => Value = value;

        public static LastName FromString(string value)
        {
            CheckValidity(value);
            return new LastName(value);
        }

        public static implicit operator string(LastName lastName) => lastName.Value;

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new UserDomainException("Last name cannot be empty");
        }
    }
}
