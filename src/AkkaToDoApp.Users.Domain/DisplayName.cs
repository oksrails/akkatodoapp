﻿using static System.String;

namespace AkkaToDoApp.Users.Domain
{
    public record DisplayName
    {
        public string Value { get; }

        internal DisplayName(string value) => Value = value;

        public static DisplayName FromString(string value)
        {
            CheckValidity(value);
            return new DisplayName(value);
        }

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new UserDomainException("Display name cannot be empty");
        }

        public static implicit operator string(DisplayName displayName) => displayName.Value;
    }
}
