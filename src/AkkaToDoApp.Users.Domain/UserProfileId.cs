﻿using AkkaToDoApp.Common.Domain;

namespace AkkaToDoApp.Users.Domain
{
    public record UserProfileId : AggregateId
    {
        public UserProfileId(Guid value) : base(value) { }

        public static UserProfileId FromGuid(Guid value) => new UserProfileId(value);
    }
}
