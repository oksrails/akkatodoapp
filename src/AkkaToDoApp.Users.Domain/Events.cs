﻿namespace AkkaToDoApp.Users.Domain
{
    public record Events
    {
        public record V1
        {
            public record UserCreated : IUserProfileEvent, IComparable<UserCreated>
            {
                public Guid UserProfileId { get; init; }
                public string FirstName { get; init; }
                public string LastName { get; init; }
                public string DisplayName { get; init; }
                public DateTime CreatedAt { get; init; }

                public UserCreated(
                    Guid userProfileId,
                    string firstName,
                    string lastName,
                    string displayName)
                {
                    UserProfileId = userProfileId;
                    FirstName = firstName;
                    LastName = lastName;
                    DisplayName = displayName;
                    CreatedAt = DateTime.Now;
                }

                public int CompareTo(UserCreated? other)
                {
                    if (ReferenceEquals(this, other)) return 0;
                    if (ReferenceEquals(null, other)) return 1;
                    return CreatedAt.CompareTo(other.CreatedAt);
                }

                public int CompareTo(IUserProfileEvent? other)
                {
                    if (other is UserCreated userCreated)
                    {
                        return CompareTo(userCreated);
                    }
                    throw new ArgumentException();
                }

                public override string ToString() => $"{nameof(UserCreated)}";
            }

            public record UserFirstNameChanged : IUserProfileEvent, IComparable<UserFirstNameChanged>
            {
                public Guid UserProfileId { get; init; }
                public string FirstName { get; init; }
                public DateTime CreatedAt { get; init; }
                public UserFirstNameChanged(
                    Guid userProfileId,
                    string firstName)
                { 
                    UserProfileId = userProfileId;
                    FirstName = firstName;
                    CreatedAt = DateTime.Now;
                }

                public int CompareTo(UserFirstNameChanged? other)
                {
                    if (ReferenceEquals(this, other)) return 0;
                    if (ReferenceEquals(null, other)) return 1;
                    return CreatedAt.CompareTo(other.CreatedAt);
                }

                public int CompareTo(IUserProfileEvent? other)
                {
                    if (other is UserFirstNameChanged changed)
                    {
                        return CompareTo(changed);
                    }
                    throw new ArgumentException();
                }

                public override string ToString() => $"{nameof(UserFirstNameChanged)}";
            }

            public record UserLastNameChanged : IUserProfileEvent, IComparable<UserLastNameChanged>
            {
                public Guid UserProfileId { get; init; }
                public string LastName { get; init; }
                public DateTime CreatedAt { get; init; }

                public UserLastNameChanged(
                    Guid userProfileId,
                    string lastName)
                {
                    UserProfileId = userProfileId;
                    LastName = lastName;
                    CreatedAt = DateTime.Now;
                }

                public int CompareTo(UserLastNameChanged? other)
                {
                    if (ReferenceEquals(this, other)) return 0;
                    if (ReferenceEquals(null, other)) return 1;
                    return CreatedAt.CompareTo(other.CreatedAt);
                }

                public int CompareTo(IUserProfileEvent? other)
                {
                    if (other is UserLastNameChanged changed)
                    {
                        return CompareTo(changed);
                    }
                    throw new ArgumentException();
                }

                public override string ToString() => $"{nameof(UserLastNameChanged)}";
            }

            public record UserDisplayNameChanged : IUserProfileEvent, IComparable<UserDisplayNameChanged>
            {
                public Guid UserProfileId { get; init; }
                public string DisplayName { get; init; }
                public DateTime CreatedAt { get; init; }

                public UserDisplayNameChanged(
                    Guid userProfileId,
                    string displayName)
                {
                    UserProfileId = userProfileId;  
                    DisplayName = displayName;
                    CreatedAt = DateTime.Now;
                }

                public int CompareTo(UserDisplayNameChanged? other)
                {
                    if (ReferenceEquals(this, other)) return 0;
                    if (ReferenceEquals(null, other)) return 1;
                    return CreatedAt.CompareTo(other.CreatedAt);
                }

                public int CompareTo(IUserProfileEvent? other)
                {
                    if (other is UserDisplayNameChanged changed)
                    {
                        return CompareTo(changed);
                    }
                    throw new ArgumentException();
                }

                public override string ToString() => $"{nameof(UserDisplayNameChanged)}";
            }
        }
    }
}
