﻿using static System.String;

namespace AkkaToDoApp.Users.Domain
{
    public record FirstName
    {
        public string Value { get; }

        internal FirstName(string value) => Value = value;

        public static FirstName FromString(string value)
        {
            CheckValidity(value);
            return new FirstName(value);
        }

        public static implicit operator string(FirstName firstName) => firstName.Value;

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new UserDomainException("First name cannot be empty");
        }
    }
}