﻿using AkkaToDoApp.Common.Domain;
using static AkkaToDoApp.Users.Domain.Events;

namespace AkkaToDoApp.Users.Domain
{
    public class UserProfile : AggregateRoot
    {
        public FirstName? FirstName { get; private set; }
        public LastName? LastName { get; private set; }
        public DisplayName? DisplayName { get; private set; }

        public UserProfile() { }

        public UserProfile(
            Guid userProfileId,
            string firstName,
            string lastName,
            string displayName)
        {
            Id = userProfileId;
            FirstName = FirstName.FromString(firstName);
            LastName = LastName.FromString(lastName);
            DisplayName = DisplayName.FromString(displayName);
        }

        protected override void EnsureValidState()
        {
            
        }

        protected override void When(object evt)
        {
            switch (evt)
            {
                case V1.UserCreated e:
                    Id = e.UserProfileId;
                    FirstName = FirstName.FromString(e.FirstName);
                    LastName = LastName.FromString(e.LastName);
                    DisplayName = DisplayName.FromString(e.DisplayName);
                    break;
                case V1.UserFirstNameChanged e:
                    FirstName = FirstName.FromString(e.FirstName);
                    break;
                case V1.UserLastNameChanged e:
                    LastName = LastName.FromString(e.LastName);
                    break;
                case V1.UserDisplayNameChanged e:
                    DisplayName = DisplayName.FromString(e.DisplayName);
                    break;
                default:
                    throw new UserDomainException($"User profile domain event {nameof(evt)} does not recognized");
            }
        }
    }
}
