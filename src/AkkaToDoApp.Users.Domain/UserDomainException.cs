﻿namespace AkkaToDoApp.Users.Domain
{
    public class UserDomainException : Exception
    {
        public UserDomainException(string message) : base(message) { }
    }
}
