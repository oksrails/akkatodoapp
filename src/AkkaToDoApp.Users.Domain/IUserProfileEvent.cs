﻿namespace AkkaToDoApp.Users.Domain
{
    public interface IUserProfileEvent : IComparable<IUserProfileEvent>
    {
        Guid UserProfileId { get; init; }
        DateTime CreatedAt { get; init; }
        string ToString();
    }
}
