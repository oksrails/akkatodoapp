﻿using AkkaToDoApp.Users;
using AkkaToDoApp.ProjectTasks;
using System.Threading.Tasks;
using AkkaToDoApp.Projects;

namespace AkkaToDoApp.ToDoService.Infrastructure
{
    public interface ICommandProcessor
    {
        void ProcessCommand(IUserProfileCommand cmd);

        void ProcessCommand(IProjectCommand cmd);

        void ProcessCommand(IProjectTaskCommand cmd);

        Task<object> ProcessQuery(IProjectIdQuery query);

        Task<object> ProcessQuery(IUserQuery query);

        Task<object> ProcessQuery(IProjectTaskIdQuery query);
    }
}
