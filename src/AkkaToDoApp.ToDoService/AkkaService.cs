﻿using Akka.Actor;
using Akka.Cluster;
using Akka.Cluster.Sharding;
using Akka.Cluster.Tools.Client;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Cluster.Tools.Singleton;
using Akka.Configuration;
using Akka.DependencyInjection;
using Akka.Persistence.MongoDb;
using Akka.Persistence.MongoDb.Query;
using Akka.Persistence.Query;
using Akka.Routing;
using AkkaToDoApp.Projects.Actors;
using AkkaToDoApp.Projects.Infrastructure;
using AkkaToDoApp.ProjectTasks.Actors;
using AkkaToDoApp.Users.Actors;
using Petabridge.Cmd.Cluster;
using Petabridge.Cmd.Cluster.Sharding;
using Petabridge.Cmd.Host;
using Petabridge.Cmd.Remote;
using Akka.Bootstrap.Docker;
using AkkaToDoApp.Users.Infrastructure;

namespace AkkaToDoApp.ToDoService
{
    /// <summary>
    /// <see cref="IHostedService"/> that runs and manages <see cref="ActorSystem"/> in background of application.
    /// </summary>
    public class AkkaService : IHostedService
    {
        private ActorSystem ClusterSystem;
        private readonly IServiceProvider _serviceProvider;
        private readonly IHostApplicationLifetime _applicationLifetime;

        public AkkaService(IServiceProvider serviceProvider, IHostApplicationLifetime applicationLifetime)
        {
            _serviceProvider = serviceProvider;
            _applicationLifetime = applicationLifetime;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var config = ConfigurationFactory.ParseString(File.ReadAllText("app.conf"));
            config = config.WithFallback(ClusterClientReceptionist.DefaultConfig()).WithFallback(DistributedPubSub.DefaultConfig()).BootstrapFromDocker();
            //config = config.WithFallback(ClusterClientReceptionist.DefaultConfig()).WithFallback(DistributedPubSub.DefaultConfig());
            var bootstrap = BootstrapSetup.Create()
               .WithConfig(config);
            var diSetup = DependencyResolverSetup.Create(_serviceProvider);
            var actorSystemSetup = bootstrap.And(diSetup);

            ClusterSystem = ActorSystem.Create("AkkaToDo", actorSystemSetup);
            MongoDbPersistence.Get(ClusterSystem);

            IActorRef userMasterActor = ClusterSystem.ActorOf(Props.Create(() => new UserMasterActor()), "userProfiles");
            IActorRef projectMasterActor = ClusterSystem.ActorOf(Props.Create(() => new ProjectMasterActor()), "projects");
            IActorRef projectTaskMasterActor = ClusterSystem.ActorOf(Props.Create(() => new ProjectTaskMasterActor()), "projectTasks");

            var readJournal = ClusterSystem.ReadJournalFor<MongoDbReadJournal>(MongoDbReadJournal.Identifier);
            IActorRef userViewMasterActor = ClusterSystem.ActorOf(Props.Create(() => new UserViewMasterActor(readJournal)).WithRouter(FromConfig.Instance), "userViews");

            Cluster.Get(ClusterSystem).RegisterOnMemberUp(() =>
            {
                var sharding = ClusterSharding.Get(ClusterSystem);

                var projectShardRegion = sharding.Start("projectAggregator",
                    s => Props.Create(() => new ProjectAggregatorActor(s, readJournal, readJournal, userViewMasterActor)),
                    ClusterShardingSettings.Create(ClusterSystem),
                    new ProjectShardMessageRouter());

                var userShardRegion = sharding.Start("userAggregator",
                    s => Props.Create(() => new UserActor(s)),
                    ClusterShardingSettings.Create(ClusterSystem),
                    new UserProfileShardMessageRouter());

                var projectSingleton = ClusterSingletonManager.Props(
                    Props.Create(() => new ProjectInitiatorActor(readJournal, projectShardRegion)),
                    ClusterSingletonManagerSettings.Create(ClusterSystem.Settings.Config.GetConfig("akka.cluster.project-singleton")));

                var userProfileClientHandler = ClusterSystem.ActorOf(Props.Create(() => new UserClientHandlerActor(userShardRegion, readJournal)), "userProfileClientHandler");
                var projectSubscriptionClientHandler = ClusterSystem.ActorOf(Props.Create(() => new ProjectClientHandlerActor(projectShardRegion, readJournal)), "projectSubscriptions");
                ClusterClientReceptionist.Get(ClusterSystem).RegisterService(userProfileClientHandler);
                ClusterClientReceptionist.Get(ClusterSystem).RegisterService(userMasterActor);
                ClusterClientReceptionist.Get(ClusterSystem).RegisterService(projectSubscriptionClientHandler);
                ClusterClientReceptionist.Get(ClusterSystem).RegisterService(projectMasterActor);
            });

            var pbm = PetabridgeCmd.Get(ClusterSystem);
            pbm.RegisterCommandPalette(ClusterCommands.Instance);
            pbm.RegisterCommandPalette(ClusterShardingCommands.Instance);
            pbm.RegisterCommandPalette(new RemoteCommands());
            pbm.Start();

            var sp = DependencyResolver.For(ClusterSystem);

            ClusterSystem.WhenTerminated.ContinueWith(tr =>
            {
                _applicationLifetime.StopApplication();
            });

            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await ClusterSystem.Terminate();
        }
    }
}
