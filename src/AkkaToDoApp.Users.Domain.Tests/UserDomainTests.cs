﻿using System;
using Xunit;
using AkkaToDoApp.Users.Domain;
using FluentAssertions;

namespace AkkaToDoApp.Users.Domain.Tests
{
    public class UserDomainTests
    {
        UserProfile _userProfile;

        public UserDomainTests()
        {
            _userProfile = new UserProfile(
                UserProfileId.FromGuid(Guid.NewGuid()),
                FirstName.FromString("First"),
                LastName.FromString("Last"),
                DisplayName.FromString("Display"));
        }

        [Fact]
        public void Change_first_name_succes_test()
        {
            var newFirstNameInput = "New";
            var firstNameChanged = new Events.V1.UserFirstNameChanged(_userProfile.Id, newFirstNameInput);
            _userProfile.Apply(firstNameChanged);

            _userProfile.FirstName?.Value.Should().Be(newFirstNameInput);
        }

        [Fact]
        public void Change_last_name_success_test()
        {
            var newLastNameInput = "New";
            var lastNameChanged = new Events.V1.UserLastNameChanged(_userProfile.Id, newLastNameInput);
            _userProfile.Apply(lastNameChanged);

            _userProfile.LastName?.Value.Should().Be(newLastNameInput);
        }

        [Fact]
        public void Change_display_name_success_test()
        {
            var newDisplayNameInput = "New";
            var displayNameChanged = new Events.V1.UserDisplayNameChanged(_userProfile.Id, newDisplayNameInput);
            _userProfile.Apply(displayNameChanged);

            _userProfile.DisplayName?.Value.Should().Be(newDisplayNameInput);
        }
    }
}
