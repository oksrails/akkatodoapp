using Xunit;
using AkkaToDoApp.Users.Domain;

namespace AkkaToDoApp.Users.Domain.Tests
{
    public class UserNameTests
    {
        [Fact]
        public void Create_first_and_last_name_success_test()
        {
            string firstNameInput = "First";
            string lastNameInput = "Last";

            var firstName = FirstName.FromString(firstNameInput);
            var lastName = LastName.FromString(lastNameInput);

            Assert.NotNull(firstName);
            Assert.NotNull(lastName);
            Assert.Equal(firstNameInput, firstName);
            Assert.Equal(lastNameInput, lastName);
        }

        [Fact]
        public void Create_first_name_with_empty_value_exception_test()
        {
            var firstNameInput = "";

            Assert.Throws<UserDomainException>(() => FirstName.FromString(firstNameInput));
        }

        [Fact]
        public void Create_last_name_with_empty_value_exception_test()
        {
            var lastNameInput = "";

            Assert.Throws<UserDomainException>(() => LastName.FromString(lastNameInput));
        }

        [Fact]
        public void Create_display_name_success_test()
        {
            var displayNameInput = "Display";

            var displayName = DisplayName.FromString(displayNameInput);

            Assert.NotNull(displayName);
            Assert.Equal(displayNameInput, displayName);
        }

        [Fact]
        public void Create_display_name_with_empty_value_excpetion_test()
        {
            var displayNameInput = "";

            Assert.Throws<UserDomainException>(() => DisplayName.FromString(displayNameInput));
        }
    }
}