﻿namespace AkkaToDoApp.Common.Subscriptions
{
    public static class TopicHelper
    {
        public static string ProjectTopic(Guid projectId)
        {
            return $"{projectId}-project";
        }

        public static string UserProfileTopic(Guid userProfileId)
        {
            return $"{userProfileId}-userProfile";
        }

        public static string ProjectTaskTopic(Guid projectTaskId)
        {
            return $"{projectTaskId}-projectTask";
        }

        public static string ProjectProjectionTopic()
        {
            return "projectProjection";
        }

        public static string UserProfileProjectionTopic()
        {
            return "userProfileProjection";
        }
    }
}