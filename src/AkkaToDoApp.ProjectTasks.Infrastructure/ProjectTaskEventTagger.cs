﻿using Akka.Persistence.Journal;
using System.Collections.Immutable;
using static AkkaToDoApp.ProjectTasks.Domain.Events;

namespace AkkaToDoApp.ProjectTasks.Infrastructure
{
    public class ProjectTaskEventTagger : IWriteEventAdapter
    {
        public string Manifest(object evt)
        {
            return string.Empty;
        }

        public object ToJournal(object evt)
        {
            switch (evt)
            {
                case V1.ProjectTaskCreated projectTaskCreated:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectTaskCreated.ProjectTaskId.ToString()).Add(projectTaskCreated.ToString()));
                case V1.ProjectTaskTitleChanged projectTaskTitleChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectTaskTitleChanged.ProjectTaskId.ToString()).Add(projectTaskTitleChanged.ToString()));
                case V1.ProjectTaskDescriptionChanged projectTaskDescriptionChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectTaskDescriptionChanged.ProjectTaskId.ToString()).Add(projectTaskDescriptionChanged.ToString()));
                case V1.ProjectTaskTypeChanged projectTaskTypeChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectTaskTypeChanged.ProjectTaskId.ToString()).Add(projectTaskTypeChanged.ToString()));
                case V1.ProjectTaskEstimationChanged projectTaskEstimationChanged:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectTaskEstimationChanged.ProjectTaskId.ToString()).Add(projectTaskEstimationChanged.ToString()));
                case V1.ProjectTaskAssigned projectTaskAssigned:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectTaskAssigned.ProjectTaskId.ToString()).Add(projectTaskAssigned.ToString()));
                case V1.ProjectTaskWorkStarted projectTaskWorkStarted:
                    return new Tagged(evt, ImmutableList<string>.Empty.Add(projectTaskWorkStarted.ProjectTaskId.ToString()).Add(projectTaskWorkStarted.ToString()));
                case V1.ProjectTaskCompleted projectTaskCompleted:
                    return new Tagged(evt, ImmutableHashSet<string>.Empty.Add(projectTaskCompleted.ProjectTaskId.ToString()).Add(projectTaskCompleted.ToString()));
                default:
                    return evt;
            }
        }
    }
}
