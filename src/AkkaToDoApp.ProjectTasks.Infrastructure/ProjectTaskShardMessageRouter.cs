﻿using Akka.Cluster.Sharding;

namespace AkkaToDoApp.ProjectTasks.Infrastructure
{
    public class ProjectTaskShardMessageRouter : HashCodeMessageExtractor, IMessageExtractor
    {
        public const int DefaultNumberOfShards = 10;

        public ProjectTaskShardMessageRouter(int maxNumberOfShards) : base(maxNumberOfShards) { }

        public ProjectTaskShardMessageRouter() : this(DefaultNumberOfShards) { }

        public override string EntityId(object message)
        {
            switch (message)
            {
                case IProjectTaskCommand projectTaskCommand:
                    return projectTaskCommand.ProjectTaskId.ToString();
                case IProjectTaskIdQuery projectTaskQuery:
                    return projectTaskQuery.ProjectTaskId.ToString();
                case ShardRegion.StartEntity se:
                    return se.EntityId;
                case ShardingEnvelope e:
                    return e.EntityId;
                default:
                    return string.Empty;
            }
        }

        public override object EntityMessage(object message)
        {
            return message;
        }

        public override string ShardId(object message)
        {
            return EntityId(message);
        }
    }
}