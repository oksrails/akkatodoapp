﻿namespace AkkaToDoApp.ProjectTasks
{
    public record QueryModels
    {
        public record FetchProjectTaskProjections : IProjectTaskQuery
        {
            public static FetchProjectTaskProjections Instance = new FetchProjectTaskProjections();
            private FetchProjectTaskProjections() {}
        }

        public record GetProjectTaskHistoryById : IProjectTaskIdQuery
        {
            public Guid ProjectTaskId { get; init; }
        }

        public record GetProjectTaskProjectionById : IProjectTaskIdQuery
        {
            public Guid ProjectTaskId { get; init; }
        }
    }
}
