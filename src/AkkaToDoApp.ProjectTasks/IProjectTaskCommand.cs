﻿namespace AkkaToDoApp.ProjectTasks
{
    public interface IProjectTaskCommand
    {
        Guid ProjectTaskId { get; init; }
    }
}
