﻿namespace AkkaToDoApp.ProjectTasks
{
    public record Commands
    {
        public record Ping
        {
            public Guid ProjectTaskId { get; init; }

            public Ping(Guid projectTaskId)
            {
                ProjectTaskId = projectTaskId;
            }
        }

        public record V1
        {
            public record FetchProjectTaskHistory : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }

                public FetchProjectTaskHistory(Guid projectTaskId)
                {
                    ProjectTaskId = projectTaskId;
                }
            }

            public record FetchProjectTaskCurrentState : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }

                public FetchProjectTaskCurrentState(Guid projectTaskId)
                {
                    ProjectTaskId = projectTaskId;
                }
            }

            public record CreateProjectTask : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }

                public CreateProjectTask(
                    Guid projectTaskId,
                    Guid projectId)
                {
                    ProjectTaskId = projectTaskId;
                    ProjectId = projectId;
                }

                public override string ToString() => $"{nameof(CreateProjectTask)}";
            }

            public record ChangeProjectTaskTitle : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public string TaskTitle { get; init; }

                public ChangeProjectTaskTitle(
                    Guid projectTaskId,
                    Guid projectId,
                    string taskTitle)
                {
                    ProjectTaskId = projectTaskId;
                    ProjectId = projectId;
                    TaskTitle = taskTitle;
                }

                public override string ToString() => $"{nameof(ChangeProjectTaskTitle)}";
            }

            public record ChangeProjectTaskDescription : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public string Description { get; init; }

                public ChangeProjectTaskDescription(
                    Guid projectTaskId,
                    Guid projectId,
                    string description)
                {
                    ProjectTaskId = projectTaskId;
                    ProjectId = projectId;
                    Description = description;
                }

                public override string ToString() => $"{nameof(ChangeProjectTaskDescription)}";
            }

            public record ChangeProjectTaskType : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public int ProjectTaskType { get; init; }

                public ChangeProjectTaskType(
                    Guid projectTaskId,
                    Guid projectId,
                    int projectTaskType)
                {
                    ProjectTaskId = projectTaskId;
                    ProjectId = projectId;
                    ProjectTaskType = projectTaskType;
                }

                public override string ToString() => $"{nameof(ChangeProjectTaskType)}";
            }

            public record ChangeProjectTaskEstimation : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public decimal ProjectTaskEstimation { get; init; }

                public ChangeProjectTaskEstimation(
                    Guid projectTaskId,
                    Guid projectId,
                    decimal projectTaskEstimation)
                {
                    ProjectTaskId = projectTaskId;
                    ProjectId = projectId;
                    ProjectTaskEstimation = projectTaskEstimation;
                }

                public override string ToString() => $"{nameof(ChangeProjectTaskEstimation)}";
            }

            public record AssignProjectTask : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }
                public Guid AssignUserId { get; init; }

                public AssignProjectTask(
                    Guid projectTaskId,
                    Guid projectId,
                    Guid assignUserId)
                {
                    ProjectTaskId = projectTaskId;
                    ProjectId = projectId;
                    AssignUserId = assignUserId;
                }

                public override string ToString() => $"{nameof(AssignProjectTask)}";
            }

            public record StartProjectTasKWork : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }

                public StartProjectTasKWork(
                    Guid projectTaskId,
                    Guid projectId)
                {
                    ProjectTaskId = projectTaskId;
                    ProjectId = projectId;
                }

                public override string ToString() => $"{nameof(StartProjectTasKWork)}";
            }

            public record CompleteProjectTask : IProjectTaskCommand
            {
                public Guid ProjectTaskId { get; init; }
                public Guid ProjectId { get; init; }

                public CompleteProjectTask(
                    Guid projectTaskId,
                    Guid projectId)
                {
                    ProjectTaskId = projectTaskId;
                    ProjectId = projectId;
                }

                public override string ToString() => $"{nameof(CompleteProjectTask)}";
            }
        }
    }
}