﻿using AkkaToDoApp.ProjectTasks.Domain;
using System.Collections.Immutable;

namespace AkkaToDoApp.ProjectTasks.Dto
{
    public class ProjectTaskHistory
    {
        public Guid ProjectTaskId { get; init; }
        public ProjectTask ProjectTaskCurentState { get; }
        public ImmutableHashSet<object> ProjectTaskEvents { get; }

        public ProjectTaskHistory(Guid projectTaskId, ImmutableHashSet<object> projectTaskEvents)
        {
            ProjectTaskId = projectTaskId;
            ProjectTaskEvents = projectTaskEvents;
            ProjectTaskCurentState = new ProjectTask();
            if (projectTaskEvents.Any())
            {
                ProjectTaskCurentState.Load(projectTaskEvents);
            }
        }

        public ProjectTaskHistory AppendProjectTaskEvent(IProjectTaskEvent projectTaskEvent)
        {
            if (!projectTaskEvent.ProjectTaskId.Equals(ProjectTaskId))
                throw new ArgumentException($"Expeccted project task id: {ProjectTaskId}, but received {projectTaskEvent.ProjectTaskId}", nameof(projectTaskEvent));

            return new ProjectTaskHistory(ProjectTaskId, ProjectTaskEvents.Add(projectTaskEvent));
        }
    }
}
