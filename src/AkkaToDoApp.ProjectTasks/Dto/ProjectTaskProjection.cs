﻿namespace AkkaToDoApp.ProjectTasks.Dto
{
    public record ProjectTaskProjection
    {
        public Guid ProjectTaskId { get; }
        public string ProjectTitle { get; }
        public string TaskTitle { get; }
        public string TaskDescription { get; }
        public string TaskType { get; }
        public string TaskEstimation { get; }
        public string TaskStatus { get; }
        public string AssignedUser { get; }

        public ProjectTaskProjection(
            Guid projectTaskId,
            string projectTitle,
            string taskTitle,
            string taskDescription,
            string taskType,
            string taskEstimation,
            string taskStatus,
            string assignedUser)
        {
            ProjectTaskId = projectTaskId;
            ProjectTitle = projectTitle;
            TaskTitle = taskTitle;
            TaskDescription = taskDescription;
            TaskType = taskType;
            TaskEstimation = taskEstimation;
            TaskStatus = taskStatus;
            AssignedUser = assignedUser;
        }
    }
}
