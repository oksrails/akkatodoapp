﻿namespace AkkaToDoApp.ProjectTasks.Dto
{
    public record ProjectTaskSnapshot
    {
        public Guid ProjectTaskId { get; init; }
        public Guid ProjectId { get; init; }
        public string? TaskTitle { get; init; }
        public string? TaskDescription { get; init; }
        public int TaskType { get; init; }
        public decimal? TaskEstimation { get; init; }
        public int TaskStatus { get; init; }
        public Guid? AssignedUserId { get; init; }
        public long QueryOffset { get; init; }

        public ProjectTaskSnapshot(
            Guid projectTaskId,
            Guid projectId,
            string taskTitle,
            string taskDescription,
            int taskType,
            decimal? taskEstimation,
            int taskStatus,
            Guid assignedUserId,
            long queryOffset)
        {
            ProjectTaskId = projectTaskId;
            ProjectId = projectId;
            TaskTitle = taskTitle;
            TaskDescription = taskDescription;
            TaskType = taskType;
            TaskEstimation = taskEstimation;
            TaskStatus = taskStatus;
            AssignedUserId = assignedUserId;
            QueryOffset = queryOffset;
        }
    }
}
