﻿namespace AkkaToDoApp.ProjectTasks
{
    public interface IProjectTaskIdQuery
    {
        public Guid ProjectTaskId { get; init; }
    }
}
